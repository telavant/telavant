import { Rule, Tree, SchematicContext, chain } from '@angular-devkit/schematics';
import { normalize } from '@angular-devkit/core';
import { getWorkspace, updateWorkspace } from '@schematics/angular/utility/config';
import { getProject } from '@schematics/angular/utility/project';
import { getServerPath, SourceQuery } from '../utility';
import { Schema } from './schema';

const CONFIG_FILE = 'smtp.json';

const DEFAULT_CONFIG = {
  endpoint: '/api/smtp',

  addresses: {
    default: {
      email: 'admin@example.test',
      name: 'Website Administrator'
    }
  },

  options: {
    host: 'localhost',
    port: 25,
    secure: false,

    auth: {
      user: '',
      pass: ''
    }
  }
};

export default function(options:Schema):Rule {
  return (host:Tree, context:SchematicContext):Rule => {
    const workspace  = getWorkspace(host);
    options.project  = options.project || workspace.defaultProject;
    const project    = getProject(host, options.project);
    const serverPath = getServerPath(host, project);

    const setupRoute:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const server = new SourceQuery(tree, serverPath);
      const router = server.queryRoutingModule();

      router.addImport('MailBackend', '@telavant/server');
      router.queryRoutes().addRoute({ path: 'api/smtp', component: 'MailBackend' });

      return tree;
    };

    const updateSMTPConfiguration = (tree:Tree, context:SchematicContext):Tree => {
      const current = tree.exists(CONFIG_FILE)
        ? JSON.parse(tree.read(CONFIG_FILE).toString('utf-8'))
        : {};

      const configuration = {
        ...DEFAULT_CONFIG,
        ...current,
        options: {
          host: options.host,
          port: options.port,
          secure: options.secure,

          auth: {
            user: options.username,
            pass: options.password
          }
        }
      };

      const content = JSON.stringify(configuration, null, 2);

      if(tree.exists(CONFIG_FILE)) {
        tree.overwrite(CONFIG_FILE, content);
      } else {
        tree.create(CONFIG_FILE, content);
      }

      return tree;
    };

    const updateWebpackConfiguration = (tree:Tree, context:SchematicContext):Tree => {
      const source = new SourceQuery(tree, normalize('/webpack.server.config.js'));
      const plugin = source.query(`//VariableDeclaration[//StringLiteral[@literalText == 'copy-webpack-plugin']]/Identifier`);

      if(!plugin.empty()) {
        const list = source.query(`//NewExpression[@name == {plugin}]/SyntaxList/ArrayLiteralExpression/SyntaxList`, { plugin: plugin.text() });

        if(list.query(`//ObjectLiteralExpression[//PropertyAssignment/StringLiteral[@literalText == {config}]]`, { config: CONFIG_FILE }).empty()) {
          list.append('\n      { from: "smtp.json", to: "conf/smtp.json" },');
        }
      }

      return tree;
    };

    return chain([
      setupRoute,
      updateSMTPConfiguration,
      updateWebpackConfiguration
    ]);
  };
}
