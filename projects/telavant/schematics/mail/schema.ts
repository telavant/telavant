import { Schema as ComponentSchema } from '@schematics/angular/component/schema';

export interface Schema extends ComponentSchema {
  /**
   * The name of the component.
   */
  name: string;

  /**
   * The name of the project.
   */
  project?: string;

  /**
   * The hostname of the SMTP server.
   */
  host?:string;

  /**
   * The SMTP port number to connect to.
   */
  port?:number;

  /**
   * Whether to use a secure connection to the SMTP server.
   */
  secure?:boolean;

  /**
   * The SMTP username to login with.
   */
  username?:string;

  /**
   * The SMTP password to login with.
   */
  password?:string;
}
