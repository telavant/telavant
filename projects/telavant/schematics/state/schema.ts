export interface Schema {
  /**
   * The name of the store.
   */
  name: string;

  /**
   * The name of the project.
   */
  project?: string;

  /**
   * The declaring NgModule.
   */
  module?: string;

  /**
   * The path to create the store.
   */
  path?: string;

  /**
   * The source root path
   */
  sourceRoot?: string;

  /**
   * When true, does not create "spec.ts" test files for the new component.
   */
  skipTests?: boolean;

  /**
   * When true, does not install packages for dependencies.
   */
  skipInstall?: boolean;

  /**
   * When true, creates the new files at the top level of the current project.
   */
  flat?: boolean;
}
