import { normalize, strings } from '@angular-devkit/core';
import { Rule, Tree, SchematicContext, apply, applyTemplates, chain, mergeWith, move, noop, url } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { getWorkspace } from '@schematics/angular/utility/config';
import { findModuleFromOptions } from '@schematics/angular/utility/find-module';
import { getProject, buildDefaultPath } from '@schematics/angular/utility/project';
import { addPackageJsonDependency, NodeDependencyType } from '@schematics/angular/utility/dependencies';
import { getAppPath, SourceQuery } from '../utility';
import { Schema } from './schema';

const NGXS_VER = '~3.5.0';

export default function(options:Schema):Rule {
  return (host:Tree, context:SchematicContext):Rule => {
    options.project  = options.project || getWorkspace(host).defaultProject;
    const project    = getProject(host, options.project);
    const sourceRoot = normalize('/' + project.sourceRoot);
    const appPath    = getAppPath(host, project);

    options = {
      ...options,
      ...project.schematics['@schematics/angular:component'],
      path: options.path ? normalize(`${sourceRoot}/${options.path}`) : buildDefaultPath(project),
    };

    const templateOptions = {
      ...options,
      ...strings,
      notFlat: (s:string) => options.flat ? '' : s,
    };

    const addDependencies:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const dependencies = [
        { name: '@ngxs/store',           version: NGXS_VER, type: NodeDependencyType.Default },
        { name: '@ngxs/form-plugin',     version: NGXS_VER, type: NodeDependencyType.Default },
        { name: '@ngxs/router-plugin',   version: NGXS_VER, type: NodeDependencyType.Default },
        { name: '@ngxs/logger-plugin',   version: NGXS_VER, type: NodeDependencyType.Default },
      ];

      dependencies.forEach(dependency => addPackageJsonDependency(tree, dependency));

      if(!options.skipInstall) {
        context.addTask(new NodePackageInstallTask());
      }

      return tree;
    };

    const createFiles:Rule = (tree:Tree, context:SchematicContext):Rule => {
      const rules:Rule[] = [
        mergeWith(apply(url('./files/state'), [
          applyTemplates(templateOptions),
          move(sourceRoot),
        ]))
      ];

      if(!options.skipTests) {
        rules.push(mergeWith(apply(url('./files/spec'), [
          applyTemplates(templateOptions),
          move(sourceRoot),
        ])));
      }

      return chain(rules);
    };

    const updateModule:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const path       = options.module ? findModuleFromOptions(tree, options) : appPath;
      const module     = new SourceQuery(host, path);
      const factory    = module.path == appPath ? 'forRoot' : 'forFeature';
      const stateClass = strings.classify(options.name) + 'State';
      const statePath  = './' + strings.dasherize(options.name) + '.state';

      module.addImport(stateClass, statePath);

      let ngxsImport = module.queryNgModuleProperty('imports')
        .query(`/CallExpression[/PropertyAccessExpression/Identifier[@text == 'NgxsModule']]`);

      if(ngxsImport.empty()) {
        // NgxsModule import missing, add it
        module.addNgImport(`NgxsModule.${factory}([ ${stateClass} ])`, '@ngxs/store');

        if(factory == 'forRoot') {
          module.addNgImport( 'NgxsFormPluginModule.forRoot()', '@ngxs/form-plugin');
          module.addNgImport( 'NgxsRouterPluginModule.forRoot()', '@ngxs/router-plugin');
          module.addNgImport( 'NgxsLoggerPluginModule.forRoot({ collapsed: true, disabled: true })', '@ngxs/logger-plugin');
        } else {
          module.addNgImport( 'NgxsFormPluginModule', '@ngxs/form-plugin');
        }
      } else if(ngxsImport.has(`/ArrayLiteralExpression/Identifier[@text == '${stateClass}']`)) {
        // stateClass is not listed, add it
        ngxsImport.query(`/ArrayLiteralExpression/Identifier[last()]`).append(', ' + stateClass);
      }

      return tree;
    };

    return chain([
      addDependencies,
      createFiles,
      updateModule
    ]);
  };
}
