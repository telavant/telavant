import { Rule, Tree, apply, url, applyTemplates, move, chain, mergeWith, MergeStrategy, SchematicContext, SchematicsException } from '@angular-devkit/schematics';
import { strings, normalize, dirname } from '@angular-devkit/core';
import { getWorkspace } from '@schematics/angular/utility/config';
import { getProject } from '@schematics/angular/utility/project';
import { buildRelativePath } from '@schematics/angular/utility/find-module';
import * as pluralize from 'pluralize';
import { getAdminPath, getAppPath, getServerPath, RouteData, SourceQuery } from '../utility';
import { Schema } from './schema';

export default function(options:Schema):Rule {
  return (host:Tree) => {
    const workspace = getWorkspace(host);
    options.project = options.project || workspace.defaultProject;

    const project   = getProject(host, options.project);
    const appPath   = getAppPath(host, project);
    const adminPath = getAdminPath(host, project);

    options.path = normalize(options.path ? `/${project.sourceRoot}/${options.path}` : dirname(appPath));

    const name  = strings.dasherize(options.name);
    const className = strings.classify(options.name);
    const path  = normalize(`${options.path}/${options.flat ? '' : name}`);
    const label = name.split('-').map(word => word[0].toUpperCase() + word.substring(1)).join(' ');

    options = {
      ...options,
      ...project.schematics['@schematics/angular:component'],
    };

    const vars = {
      ...options,
      ...strings,
      notFlat:   s => options.flat ? '' : s,

      label,
      className,
      single:   strings.camelize(options.name),
      multiple: pluralize(strings.camelize(options.name), 2),
      uri:      `/${options.backendPath}/${name}`,

      baseName:   name,
      listName:   name + '-list',
      detailName: name + '-detail',
      adminName:  name + '-admin',
      formName:   name + '-form',

      resolverClass: className + 'Resolver',
      serviceClass:  className + 'Service',
      backendClass:  className + 'BackendComponent',
      listClass:     className + 'ListComponent',
      detailClass:   className + 'DetailComponent',
      adminClass:    className + 'AdminComponent',
      formClass:     className + 'FormComponent',
    };


    const installTemplates:Rule = (tree:Tree, context:SchematicContext):Rule => {
      const admin     = new SourceQuery(tree, adminPath);
      const actions   = admin.queryStateModule().path.replace(/\.state\.ts$/, '.actions');
      const component = normalize(`${path}/${vars.adminName}.component`);
      const actionsPath = buildRelativePath(component, actions);

      let rules = [
        mergeWith(apply(url('./files/basic'), [
          applyTemplates({ ...vars, actionsPath }),
          move(options.path)
        ])),
      ];

      if(options.ui != 'basic') {
        rules.push(mergeWith(apply(url('./files/' + options.ui), [
          applyTemplates({ ...vars, actionsPath }),
          move(options.path)
        ]), MergeStrategy.Overwrite));
      }

      return chain(rules);
    };

    const setupEntity:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const entity = normalize(`${path}/${name}.entity`);
      const barrel = normalize(`/${project.sourceRoot}/${options.entities}`);
      const source = new SourceQuery(tree, barrel);

      source.addExport('*', buildRelativePath(barrel, entity), true);

      return tree;
    };

    const setupFrontend:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const app = new SourceQuery(tree, appPath);
      const router = app.queryRoutingModule();

      const resolverFile = normalize(`${path}/${name}.resolver`);
      const listFile     = normalize(`${path}/${vars.listName}.component`);
      const detailFile   = normalize(`${path}/${vars.detailName}.component`);

      // register components with app module

      app.addNgDeclaration(vars.listClass,   buildRelativePath(app.path, listFile))
         .addNgDeclaration(vars.detailClass, buildRelativePath(app.path, detailFile));

      // set up routes

      router.addImport(vars.resolverClass, buildRelativePath(router.path, resolverFile))
            .addImport(vars.listClass,     buildRelativePath(router.path, listFile))
            .addImport(vars.detailClass,   buildRelativePath(router.path, detailFile));

      router.queryRoutes()
        .addRoute({ path: pluralize(name) + '/:id', component: vars.detailClass, resolve: `{ ${name}: ${vars.resolverClass} }` }, '')
        .addRoute({ path: pluralize(name),          component: vars.listClass,   resolve: `{ ${pluralize(name)}: ${vars.resolverClass} }` }, '');

      return tree;
    };

    const setupBackend:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const server       = new SourceQuery(tree, getServerPath(tree, project));
      const router       = server.queryRoutingModule();
      const backendFile  = normalize(`${path}/${name}.backend`);
      const resolve      = '{ params: DataResolver }';

      // register component with server module

      server.addNgDeclaration(vars.backendClass, buildRelativePath(server.path, backendFile));

      // set up backend routes

      router.addImport('DataResolver', '@telavant/platform')
            .addImport(vars.backendClass, buildRelativePath(router.path, backendFile));

      const route:RouteData = { path: name, component: vars.backendClass, resolve };

      router.queryRoutes()
        .addRoute(route, options.backendPath)
        .addRoute({ ...route, path: route.path + '/:id' }, options.backendPath);

      return tree;
    };

    const setupAdmin:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const admin = new SourceQuery(tree, getAdminPath(tree, project));

      const resolverFile = normalize(`${path}/${name}.resolver`);
      const adminFile    = normalize(`${path}/${vars.adminName}.component`);
      const formFile     = normalize(`${path}/${vars.formName}.component`);

      // register components with admin module

      admin.addNgDeclaration(vars.adminClass, buildRelativePath(admin.path, adminFile))
           .addNgDeclaration(vars.formClass,  buildRelativePath(admin.path, formFile));

      // set up admin routes

      const router = admin.queryRoutingModule();

      router.addImport(vars.resolverClass, buildRelativePath(router.path, resolverFile))
            .addImport(vars.adminClass,    buildRelativePath(router.path, adminFile))
            .addImport(vars.formClass,     buildRelativePath(router.path, formFile));

      router.queryRoutes()
        .addRoute({ path: pluralize(name) + '/:id', component: vars.formClass,  resolve: `{ ${name}: ${vars.resolverClass} }` }, '')
        .addRoute({ path: pluralize(name),          component: vars.adminClass, resolve: `{ ${pluralize(name)}: ${vars.resolverClass} }` }, '');

      // set up state management

      const state      = admin.queryStateModule();
      const stateClass = state.queryClassByDecorator('State');
      const defaults   = stateClass.queryDecoratorProperty('defaults');
      const selectors  = stateClass.queryMethodByDecorator('Selector');
      const model      = stateClass.queryDecoratorType().text();

      state.queryInterface(model).queryBody().append(`\n  ${options.name}: PageState;`);
      defaults.query(`/ObjectLiteralExpression/SyntaxList`).append(`\n    ${options.name}: defaultState,`);

      let selector = `\n  @Selector() static ${options.name}(state:${model}) { return state.${options.name}; }`;

      if(selectors.empty()) {
        stateClass.queryBody().prepend(selector + '\n');
      } else {
        selectors.query(`/MethodDeclaration[last()]`).append(selector);
      }

      stateClass.query(`
        //MethodDeclaration[/SyntaxList/Decorator[@name == {decorator}]
        /CallExpression/SyntaxList/Identifier[@text == {action}]]
        //CallExpression[@name == {function}]/SyntaxList
        /ObjectLiteralExpression/SyntaxList
      `, {
        decorator: 'Action',
        action:    'ResetStateAction',
        function:  'setState'
      }).append(`\n      ${options.name}: defaultState,`);

      // set up admin navigation

      const component = new SourceQuery(tree, normalize(admin.path.replace(/\.module\.ts$/, '.component.ts')));

      const nav = component.query(`
        //PropertyDeclaration[/ArrayType[@text == 'AdminNavigation[]']]
        /ArrayLiteralExpression/SyntaxList
      `);

      const indent = nav.text().trim().length == 0 ? '\n    ' : '  ';

      nav.append(`${indent}{ path: '${vars.multiple}', icon: 'folder', label: '${pluralize(label)}' },\n  `);

      return tree;
    };

    return chain([
      installTemplates,
      setupEntity,
      setupFrontend,
      setupBackend,
      setupAdmin,
    ]);
  };
}
