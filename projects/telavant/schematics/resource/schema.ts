import { Schema as ComponentSchema } from '@schematics/angular/component/schema';

export interface Schema extends ComponentSchema {
  /**
   * The path to the entities barrel.
   */
  entities?: string;

  /**
   * The base URI for backend routes.
   */
  backendPath?: string;

  /**
   * When true, does not install packages for dependencies.
   */
  ui: 'basic' | 'clarity';

  /**
   * When true, does not install packages for dependencies.
   */
  skipInstall?: boolean;
}
