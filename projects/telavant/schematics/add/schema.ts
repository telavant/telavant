import { Schema as UniversalSchema } from '@schematics/angular/universal/schema';

export interface Schema extends UniversalSchema {
  /**
   * The name of the server application folder.
   */
  serverDir?: string;

  /**
   * The path to the entities barrel.
   */
  entities?: string;

  /**
   * The path to use for backend routes.
   */
  backendPath?: string;

  /**
   * When true, does not an admin module.
   */
  skipAdmin?: boolean;

  /**
   * The path to use for the admin folder.
   */
  adminDir?: string;

  /**
   * The uri path to use for admin routes.
   */
  adminPath?: string;

  /**
   * The name of the admin dashboard component (default: 'dashboard')
   */
  dashboardName?: string;

  /**
   * When true, does not create "spec.ts" test files for the new component.
   */
  skipTests?: boolean;

  /**
   * When true, creates the new files at the top level of the current project.
   */
  flat?: boolean;

  /**
   * When true, removes the AppComponent placeholder template.
   */
  removePlaceholder?: boolean;
}
