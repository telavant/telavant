import { dirname, join, JsonAstArray, JsonAstObject, normalize, parseJsonAst, strings } from '@angular-devkit/core';
import { apply, applyTemplates, chain, externalSchematic, MergeStrategy, mergeWith, move, noop, Rule, SchematicContext, Tree, url } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { getWorkspace, updateWorkspace } from '@schematics/angular/utility/config';
import { addPackageJsonDependency, NodeDependencyType } from '@schematics/angular/utility/dependencies';
import { buildRelativePath } from '@schematics/angular/utility/find-module';
import { findPropertyInAstObject } from '@schematics/angular/utility/json-utils';
import { getProject } from '@schematics/angular/utility/project';
import { addPackageJsonBrowserModule, addPackageJsonScript, appendPropertyInAstObject, appendValuesInAstArray, getAppPath, getServerPath, SourceQuery, updatePropertyInAstObject } from '../utility';
import { Schema } from './schema';

const NG_VER   = '^9.0.0';
const CLR_VER  = 'next';
const TELAVANT_VER = 'latest';

export default function(options:Schema):Rule {
  return (host:Tree, context:SchematicContext) => {
    let workspace         = getWorkspace(host);
    options.clientProject = options.clientProject || workspace.defaultProject;
    options.appId         = options.appId == 'serverApp' ? options.clientProject : options.appId;
    let project           = getProject(host, options.clientProject);
    options               = { ...options, ...project.schematics['@schematics/angular:component'] };

    const projectRoot     = normalize('/' + project.root);
    const sourceRoot      = normalize('/' + project.sourceRoot);
    const tsconfig        = normalize(`${project.root}/${options.tsconfigFileName}.json`);
    const appPath         = getAppPath(host, project);

    const addDependencies:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const dependencies = [
        { name: '@nguniversal/common',                      version: NG_VER, type: NodeDependencyType.Default },
        { name: '@nguniversal/express-engine',              version: NG_VER, type: NodeDependencyType.Default },

        { name: '@telavant/astq',     version: TELAVANT_VER, type: NodeDependencyType.Dev },
        { name: '@telavant/platform', version: TELAVANT_VER, type: NodeDependencyType.Default },
        { name: '@telavant/server',   version: TELAVANT_VER, type: NodeDependencyType.Default },

        { name: '@webcomponents/custom-elements', version: '^1.0.0',  type: NodeDependencyType.Default },
        { name: 'chokidar',                       version: '^3.3.0',  type: NodeDependencyType.Default },
        { name: 'class-transformer',              version: '^0.2.0',  type: NodeDependencyType.Default },
        { name: 'cors',                           version: '^2.8.5',  type: NodeDependencyType.Default },
        { name: 'mariadb',                        version: '^2.2.0',  type: NodeDependencyType.Default },
        { name: 'pluralize',                      version: '^7.0.0',  type: NodeDependencyType.Default },
        { name: 'pm2',                            version: '^4.1.2',  type: NodeDependencyType.Default },
        { name: 'preboot',                        version: '^7.0.0',  type: NodeDependencyType.Default },
        { name: 'rxjs',                           version: '~6.5.0',  type: NodeDependencyType.Default },
        { name: 'nodemailer',                     version: '^6.0.0',  type: NodeDependencyType.Default },
        { name: 'tslib',                          version: '^1.10.0', type: NodeDependencyType.Default },
        { name: 'typeorm',                        version: '^0.2.0',  type: NodeDependencyType.Default },

        { name: '@angular-builders/custom-webpack', version: '^9.0.0-beta.7', type: NodeDependencyType.Dev },
        { name: '@types/express-serve-static-core', version: '^4.16.3',       type: NodeDependencyType.Dev },
        { name: '@types/nodemailer',                version: '^6.0.0',        type: NodeDependencyType.Dev },
        { name: 'copy-webpack-plugin',              version: '^5.1.1',        type: NodeDependencyType.Dev },
        { name: 'webpack-filter-warnings-plugin',   version: '^1.2.1',        type: NodeDependencyType.Dev }
      ];

      dependencies.forEach(dependency => addPackageJsonDependency(tree, dependency));

      addPackageJsonBrowserModule(tree, 'http', false);
      addPackageJsonBrowserModule(tree, 'https', false);
      addPackageJsonBrowserModule(tree, 'net', false);
      addPackageJsonBrowserModule(tree, 'path', false);
      addPackageJsonBrowserModule(tree, 'stream', false);
      addPackageJsonBrowserModule(tree, 'tls', false);
      addPackageJsonBrowserModule(tree, 'koa', false);
      addPackageJsonBrowserModule(tree, 'koa-bodyparser', false);
      addPackageJsonBrowserModule(tree, 'koa-multer', false);
      addPackageJsonBrowserModule(tree, 'koa-router', false);
      addPackageJsonBrowserModule(tree, 'kcors', false);
      addPackageJsonBrowserModule(tree, 'react-native-sqlite-storage', false);

      return tree;
    };

    const setupClarity:Rule = (tree:Tree, context:SchematicContext):Rule => {
      // ng add @clr/angular doesn't honor --skip-install
      //return externalSchematic('@clr/angular', 'ng-add', { project: options.clientProject });

      const dependencies = [
        { name: '@clr/core',    version: CLR_VER, type: NodeDependencyType.Default },
        { name: '@clr/angular', version: CLR_VER, type: NodeDependencyType.Default },
        { name: '@clr/icons',   version: CLR_VER, type: NodeDependencyType.Default },
        { name: '@clr/ui',      version: CLR_VER, type: NodeDependencyType.Default },
      ];

      dependencies.forEach(dependency => addPackageJsonDependency(tree, dependency));

      const app = new SourceQuery(tree, appPath);

      app.addNgImport('BrowserAnimationsModule', '@angular/platform-browser/animations');
      app.addNgImport('ClarityModule', '@clr/angular');

      const buildOptions = project.architect.build.options;
      const scriptsSearch = buildOptions.scripts.join('|');
      const stylesSearch = buildOptions.styles.join('|');

      if(stylesSearch.search('node_modules/@telavant/platform/styles/clarity-full-width-form-elements') < 0) {
        buildOptions.styles.unshift('node_modules/@telavant/platform/styles/clarity-full-width-form-elements.css');
      }

      if(stylesSearch.search('node_modules/@clr/ui/clr-ui') < 0) {
        buildOptions.styles.unshift('node_modules/@clr/ui/clr-ui.min.css');
      }

      if(stylesSearch.search('node_modules/@clr/icons/clr-icons') < 0) {
        buildOptions.styles.unshift('node_modules/@clr/icons/clr-icons.min.css');
      }

      if(stylesSearch.search('node_modules/@clr/core/global') < 0) {
        buildOptions.styles.unshift('node_modules/@clr/core/global.min.css');
      }

      if(scriptsSearch.search('node_modules/@clr/icons/clr-icons.min.js') < 0) {
        buildOptions.scripts.push('node_modules/@clr/icons/clr-icons.min.js');
      }

      if(scriptsSearch.search('node_modules/@webcomponents/custom-elements/custom-elements.min.js') < 0) {
        // Want this first
        buildOptions.scripts.unshift('node_modules/@webcomponents/custom-elements/custom-elements.min.js');
      }

      workspace.projects[options.clientProject].architect.build.options = buildOptions;
      return updateWorkspace(workspace);
    };

    const setupUniversal:Rule = (tree:Tree, context:SchematicContext):Rule => {
      let rules = [];

      if(tree.exists(tsconfig)) {
        if(!options.skipInstall) {
          context.addTask(new NodePackageInstallTask());
        }
      } else {
        rules.push(externalSchematic('@schematics/angular', 'universal', options));
      }

      return chain(rules);
    };

    const moveServerModule:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const serverPath = getServerPath(tree, tsconfig);
      const server = new SourceQuery(tree, serverPath);
      const oldPath = server.path;
      const newPath = normalize(`${sourceRoot}/${options.serverDir}/${options.rootModuleFileName}`);

      server.rename(newPath);

      const jsonAst = parseJsonAst(tree.read(tsconfig).toString('utf-8')) as JsonAstObject;
      const angularCompilerOptions = findPropertyInAstObject(jsonAst, 'angularCompilerOptions') as JsonAstObject;
      const entryModule = findPropertyInAstObject(angularCompilerOptions, 'entryModule') as JsonAstObject;
      const newEntryModule = entryModule.value.toString().replace(oldPath.replace(/\.ts$/, ''), newPath.replace(/\.ts$/, ''));
      const recorder = tree.beginUpdate(tsconfig);

      updatePropertyInAstObject(recorder, angularCompilerOptions, 'entryModule', newEntryModule, 4);
      tree.commitUpdate(recorder);

      return tree;
    };

    const updateServerModule:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const serverPath = getServerPath(tree, tsconfig);
      const server = new SourceQuery(tree, serverPath);

      server.queryNgModuleProperty('imports')
        .query(`/SyntaxList/Identifier[@text == 'ServerModule']`)
        .replace('TelavantServerModule');

      server.queryImported('ServerModule', '@angular/platform-server').delete();
      server.addImport('RootComponent', '@telavant/platform');
      server.addImport('TelavantServerModule', '@telavant/server');
      server.addNgImport('TelavantServerModule', '@telavant/server');
      server.queryNgModuleProperty('bootstrap').query('/SyntaxList').replace('RootComponent');

      return tree;
    };

    const updateAppModule:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const app = new SourceQuery(tree, appPath);

      app.addNgImport(`BrowserModule.withServerTransition({ appId: '${options.appId}' })`, '@angular/platform-server')
         .addNgImport('TransferHttpCacheModule', '@nguniversal/common')
         .addNgImport(`PrebootModule.withConfig({ appRoot: 'app-root' })`, 'preboot')
         .addNgImport('TelavantPlatformModule', '@telavant/platform');

      app.addImport('RootComponent', '@telavant/platform');
      app.queryNgModuleProperty('bootstrap').query('/SyntaxList').replace('RootComponent');

      return tree;
    };

    const updateAppComponent:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const path = normalize(appPath.replace(/\.module\.ts$/, '.component'));
      const component = new SourceQuery(tree, normalize(path + '.ts'));
      const template = path + '.html';

      component.queryClassByDecorator('Component')
        .queryDecoratorProperty('selector')
        .query('/StringLiteral')
        .replace(" 'app-component'");

      if(options.removePlaceholder && tree.exists(template)) {
        let content = tree.read(template).toString('utf-8');

        if(content.match(/<!--(\s\*)+ The content below (\*\s)+-->/)) {
          tree.overwrite(template, content.replace(/<!-- (\*\s)+-->.*<!-- (\*\s)+-->/ms, '').trim());
        }
      }

      return tree;
    };

    const createNotFoundComponent:Rule = (tree:Tree, context:SchematicContext):Rule => {
      const app = new SourceQuery(tree, appPath);
      const notfoundFile = normalize(`${dirname(appPath)}/not-found.component`);

      app.addNgDeclaration('NotFoundComponent', buildRelativePath(app.path, notfoundFile));

      return mergeWith(
        apply(url('./files/notfound'), [
          applyTemplates({ ...strings, ...options }),
          move(dirname(appPath)),
        ]), MergeStrategy.Overwrite
      );
    };

    const setupDefaultRoutes:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const app = new SourceQuery(tree, appPath);
      const router = app.queryRoutingModule();
      const appFile = appPath.replace(/\.module\.ts$/, '.component');
      const notfoundFile = normalize(`${dirname(appPath)}/not-found.component`);

      router.addImport('AppComponent',      buildRelativePath(router.path, appFile))
            .addImport('NotFoundComponent', buildRelativePath(router.path, notfoundFile));

      router.queryRoutes()
        .addRoute({ path: '',   component: 'AppComponent', children: [] })
        .addRoute({ path: '**', component: 'NotFoundComponent' });

      return tree;
    };

    const setupCustomWebpack:Rule = (tree:Tree, context:SchematicContext):Rule => {
      workspace = getWorkspace(tree);

      let config = {
        mergeStrategies: { loaders: 'replace' },
        replaceDuplicatePlugins: true
      };

      let buildTarget = workspace.projects[options.clientProject].architect.build as any;
      buildTarget.builder = '@angular-builders/custom-webpack:browser';
      buildTarget.options.outputPath = 'dist/public';
      buildTarget.options.customWebpackConfig = { path: 'webpack.app.config.js', ...config };
      buildTarget.options.extractCss = true;
      buildTarget.options.sourceMap = true;
      delete buildTarget.options.es5BrowserSupport;
      workspace.projects[options.clientProject].architect.build = buildTarget;

      let serverTarget = workspace.projects[options.clientProject].architect.server as any;
      serverTarget.builder = '@angular-builders/custom-webpack:server';
      serverTarget.options.outputPath = 'dist';
      serverTarget.options.deleteOutputPath = false;
      serverTarget.options.customWebpackConfig = { path: 'webpack.server.config.js', ...config };
      workspace.projects[options.clientProject].architect.server = serverTarget;

      return updateWorkspace(workspace);
    };

    const createWebpackConfigs:Rule = (tree:Tree, context:SchematicContext):Rule => mergeWith(
      apply(url('./files/webpack'), [
        applyTemplates({ ...strings, ...options }),
        move(projectRoot),
      ]), MergeStrategy.Overwrite
    );

    const updateServerTsConfig:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const jsonAst = parseJsonAst(tree.read(tsconfig).toString('utf-8')) as JsonAstObject;
      const compilerOptions = findPropertyInAstObject(jsonAst, 'compilerOptions') as JsonAstObject;
      const angularCompilerOptions = findPropertyInAstObject(jsonAst, 'angularCompilerOptions') as JsonAstObject;
      const files = findPropertyInAstObject(jsonAst, 'files') as JsonAstArray;
      const entryModule = findPropertyInAstObject(angularCompilerOptions, 'entryModule') as JsonAstObject;
      const entryRegex = new RegExp('^' + options.appDir + '/');
      const newEntryModule = entryModule.value.toString().replace(entryRegex, options.serverDir + '/');
      const recorder = tree.beginUpdate(tsconfig);

      appendPropertyInAstObject(recorder, compilerOptions, 'paths', {
        entities: [join(sourceRoot, options.entities).substr(1)]
      }, 4);

      appendValuesInAstArray(recorder, files, [
        join(sourceRoot, 'server.ts').substr(1)
      ], 4);

      updatePropertyInAstObject(recorder, angularCompilerOptions, 'entryModule', newEntryModule, 4);

      tree.commitUpdate(recorder);
      return tree;
    };

    const updateAppTsConfig:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const serverDir = normalize(options.serverDir);
      const appTsConfig = normalize(`${projectRoot}/tsconfig.app.json`);
      const jsonAst = parseJsonAst(tree.read(appTsConfig).toString('utf-8')) as JsonAstObject;
      const compilerOptions = findPropertyInAstObject(jsonAst, 'compilerOptions') as JsonAstObject;
      const exclude = findPropertyInAstObject(jsonAst, 'exclude') as JsonAstArray;
      const recorder = tree.beginUpdate(appTsConfig);

      appendPropertyInAstObject(recorder, compilerOptions, 'paths', {
        typeorm:  ['node_modules/typeorm/browser'],
        entities: [join(sourceRoot, options.entities).substr(1)]
      }, 4);

      tree.commitUpdate(recorder);
      return tree;
    };

    const createServerRoutingModule:Rule = (tree:Tree, context:SchematicContext):Rule => {
      if(tree.exists(normalize(`${sourceRoot}/${options.serverDir}/server-routing.module.ts`))) {
        return noop();
      }

      return mergeWith(apply(url('./files/routing'), [
        applyTemplates({ ...strings, ...options }),
        move(sourceRoot),
      ]), MergeStrategy.Overwrite);
    };

    const importServerRoutingModule:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const serverPath = getServerPath(tree, tsconfig);
      const server = new SourceQuery(tree, serverPath);

      server.addImport('ServerRoutingModule', './server-routing.module');

      server.queryNgModuleProperty('imports').query('/SyntaxList').prepend('\n    ServerRoutingModule,');

      return tree;
    };

    const createEntitiesBarrel:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const barrel = normalize(`${sourceRoot}/${options.entities}.ts`);

      if(!tree.exists(barrel)) {
        tree.create(barrel, '');
      }

      return tree;
    };

    const createAdminModule:Rule = (tree:Tree, context:SchematicContext):Rule => {
      if(options.skipAdmin) {
        return noop();
      }

      return externalSchematic('@telavant/schematics', 'admin', {
        ...options,
        name: 'admin'
      });
    };

    const createAppConfig:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const config = 'config.json';

      if(!tree.exists(config)) {
        tree.create(config, `{\n  "name": "${options.appId}"\n}`);
      }

      return tree;
    };

    const createDatabaseConfig:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const ormconfig = 'ormconfig.json';

      if(!tree.exists(ormconfig)) {
        tree.create(ormconfig, '{}');
      }

      return tree;
    };

    const createMainServerScript:Rule = (tree:Tree, context:SchematicContext):Rule => {
      const mainPath = normalize(`${sourceRoot}/${options.main}`);
      const serverModulePath = buildRelativePath(normalize(`${sourceRoot}/main.server.ts`), getServerPath(tree, tsconfig).replace(/\.ts$/, ''));

      if(tree.exists(mainPath)) {
        tree.delete(mainPath);
      }

      return mergeWith(apply(url('./files/server'), [
        applyTemplates({ ...strings, ...options, serverModulePath }),
        move(sourceRoot),
      ]), MergeStrategy.Overwrite);
    };

    const setDefaultCollection:Rule = (tree:Tree, context:SchematicContext) => {
      workspace = getWorkspace(tree);
      return updateWorkspace({ ...workspace, cli: { defaultCollection: '@telavant/schematics' }});
    };

    const setupScripts:Rule = (tree:Tree, context:SchematicContext):Rule => {
      addPackageJsonScript(tree, 'start',         'pm2 start ecosystem.config.js && npm run logs');
      addPackageJsonScript(tree, 'restart',       'pm2 restart build.js && npm run logs');
      addPackageJsonScript(tree, 'logs',          'pm2 logs --raw --lines 0');
      addPackageJsonScript(tree, 'stop',          'pm2 stop ecosystem.config.js');
      addPackageJsonScript(tree, 'build',         'npm run build:browser && npm run build:server');
      addPackageJsonScript(tree, 'build:browser', 'ng build');
      addPackageJsonScript(tree, 'build:server',  `ng run ${options.clientProject}:server`);

      return mergeWith(apply(url('./files/scripts'), [
        applyTemplates({ ...strings, ...options }),
        move(projectRoot),
      ]), MergeStrategy.Overwrite);
    };

    return chain([
      addDependencies,
      setupClarity,
      setupUniversal,
      moveServerModule,
      updateServerModule,
      updateServerTsConfig,
      updateAppModule,
      updateAppComponent,
      updateAppTsConfig,
      setupCustomWebpack,
      createWebpackConfigs,
      createServerRoutingModule,
      importServerRoutingModule,
      createEntitiesBarrel,
      createAdminModule,
      createNotFoundComponent,
      setupDefaultRoutes,
      createAppConfig,
      createDatabaseConfig,
      createMainServerScript,
      setDefaultCollection,
      setupScripts
    ]);
  };
}
