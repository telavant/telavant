module.exports = {
  apps : [
    {
      name: 'server.js',
      script: './server.js',
      args: '',
      instances: 1,
      // exec_mode: 'fork',
      autorestart: true,
      restart_delay: 2500,
      max_restarts: 1000,
      exp_backoff_restart_delay: 100,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    },

    {
      name: 'build.js',
      script: './build.js',
      args: '',
      instances: 1,
      // exec_mode: 'fork',
      max_memory_restart: '1G',
    },
  ],

  deploy : {
    production : {
      user : 'node',
      host : '127.0.0.1',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
