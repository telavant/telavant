module.exports = {
  node: { crypto: true, stream: true },

  resolve: {
    alias: {
      typeorm$: 'typeorm/browser',
    }
  },
};
