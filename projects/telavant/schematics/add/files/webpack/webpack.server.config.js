const FilterWarnings = require('webpack-filter-warnings-plugin');
const CopyFiles = require('copy-webpack-plugin');

module.exports = {
  node: { crypto: true, stream: true, __dirname: false },

  entry: {
    main: './src/main.server.ts',
    server: './src/server.ts'
  },

  plugins: [
    new FilterWarnings({
      exclude: /(the request of a dependency is an expression|Can't resolve.*(fsevents|typeorm))/
    }),

    new CopyFiles([
      { from: "ecosystem.config.js", to: "ecosystem.config.js" },
      { from: "config.json", to: "conf/config.json" },
      { from: "ormconfig.json", to: "conf/ormconfig.json" },
    ])
  ]
};
