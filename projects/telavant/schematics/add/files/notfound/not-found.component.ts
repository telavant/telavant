import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformServer } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseService } from '@telavant/platform';

@Component({
  selector: 'app-notfound',
  template: '<h1>Page Not Found</h1><p>The resource at <code>{{ router.url }}</code> could not be located.</p>',
})
export class NotFoundComponent implements OnInit {
  constructor(@Inject(PLATFORM_ID) private platformId, public router:Router, private response:ResponseService) { }

  ngOnInit() {
    if(isPlatformServer(this.platformId)) {
      this.response.setNotFound();
    }
  }
}
