import 'zone.js/dist/zone';
import { existsSync, readFileSync } from 'fs';
import { join, normalize, dirname } from 'path';

const domino = require('domino');
const index  = normalize(join(__dirname, 'public/index.html'));
const server = normalize(join(__dirname, 'main.js'));

if(!existsSync(index)) {
  console.error("Browser build not found at " + dirname(index));
  process.exit(1);
}

if(!existsSync(server)) {
  console.error("Server build not found at " + server);
  process.exit(1);
}

const win = domino.createWindow(readFileSync(index).toString());

global['window'] = win;
global['Node'] = win.Node;
global['navigator'] = win.navigator;
global['Event'] = win.Event;
global['KeyboardEvent'] = win.Event;
global['MouseEvent'] = win.Event;
global['document'] = win.document;
global['Document'] = class Document {};
global['HTMLDocument'] = win.HTMLDocument;
global['HTMLInputElement'] = win.HTMLInputElement;
global['HTMLElement'] = class HTMLElement {};
global['HTMLAnchorElement'] = class HTMLAnchorElement {};
global['HTMLButtonElement'] = class HTMLButtonElement {};
global['JSCompiler_renameProperty'] = (a, b) => a;
global['File'] = null;

process.chdir(__dirname);

declare const __non_webpack_require__:any;
__non_webpack_require__(__dirname + '/main');
