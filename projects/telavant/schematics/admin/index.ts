import { dirname, normalize, strings } from '@angular-devkit/core';
import { apply, applyTemplates, chain, externalSchematic, MergeStrategy, mergeWith, move, Rule, schematic, SchematicContext, SchematicsException, Tree, url } from '@angular-devkit/schematics';
import { Implement } from '@schematics/angular/guard/schema';
import { getWorkspace, updateWorkspace } from '@schematics/angular/utility/config';
import { addPackageJsonDependency, NodeDependencyType } from '@schematics/angular/utility/dependencies';
import { buildRelativePath } from '@schematics/angular/utility/find-module';
import { getProject } from '@schematics/angular/utility/project';
import * as pluralize from 'pluralize';
import { getAppPath, SourceQuery } from '../utility';
import { Schema } from './schema';

export default function(options:Schema):Rule {
  return (host:Tree) => {
    let workspace = getWorkspace(host);
    options.project = options.project || workspace.defaultProject;
    const project = getProject(host, options.project);
    const sourceRoot = normalize('/' + project.sourceRoot);
    const appPath = getAppPath(host, project);

    options = {
      ...options,
      ...project.schematics['@schematics/angular:component'],
      path: options.path ? normalize(`${sourceRoot}/${options.path}`) : sourceRoot,
      adminPath: options.adminPath || options.name
    };

    const adminRoot = normalize(`${options.path}/${options.flat ? '' : strings.dasherize(options.name)}`);
    const adminPath = normalize(`${adminRoot}/${strings.dasherize(options.name)}.module.ts`);

    const templateOptions = {
      ...options,
      ...strings,
      pluralize: (s:string) => pluralize(s, 2),
      notFlat: (s:string) => options.flat ? '' : s,
      routing: strings.dasherize(options.name) + '-routing',
    };

    const addDependencies:Rule = (tree:Tree, context:SchematicContext):Tree => {
      [
        { name: '@clr/angular', version: '^2.0.0', type: NodeDependencyType.Default },
        { name: '@clr/icons', version: '^2.0.0', type: NodeDependencyType.Default },
        { name: '@clr/ui', version: '^2.0.0', type: NodeDependencyType.Default },
        { name: 'angular-pipes', version: '^8.0.0', type: NodeDependencyType.Default },
        { name: 'ngx-image-cropper', version: '^1.0.1', type: NodeDependencyType.Default },
        { name: 'ngx-toastr', version: '^10.0.0', type: NodeDependencyType.Default },
        { name: 'ngx-uploader', version: '^7.0.0', type: NodeDependencyType.Default },
        { name: 'ngx-quill', version: '^4.6.9', type: NodeDependencyType.Default },
        { name: 'quill', version: '^1.3.6', type: NodeDependencyType.Default },
      ].forEach(dependency => addPackageJsonDependency(tree, dependency));

      return tree;
    };

    const setupToastr:Rule = (tree:Tree, context:SchematicContext):Rule => {
      const app = new SourceQuery(tree, appPath);

      app.addNgImport('BrowserAnimationsModule', '@angular/platform-browser/animations');
      app.addNgImport('ToastrModule.forRoot()', 'ngx-toastr');

      const buildOptions = project.architect.build.options;
      const stylesSearch = buildOptions.styles.join('|');

      if(stylesSearch.search('node_modules/ngx-toastr') < 0) {
        buildOptions.styles.unshift('node_modules/ngx-toastr/toastr.css');
      }

      workspace.projects[options.project].architect.build.options = buildOptions;
      return updateWorkspace(workspace);
    };

    const setupAppState:Rule = (tree:Tree, context:SchematicContext):Rule => {
      const appRoot = dirname(appPath);

      const stateOptions = {
        name: 'app',
        path: buildRelativePath(normalize(`${sourceRoot}/main.ts`), appRoot).replace(/^\.\//, ''),
        module: 'app.module.ts',
        skipTests: options.skipTests,
        skipInstall: options.skipInstall,
        flat: options.flat
      };

      return schematic('state', stateOptions);
    };

    const createAdminModule:Rule = (tree:Tree, context:SchematicContext):Rule => {
      return externalSchematic('@schematics/angular', 'module', {
        name: options.name,
        path: options.path,
        flat: options.flat,
        lintFix: options.lintFix,
        project: options.project,
        module: buildRelativePath(adminRoot, appPath),
        route: options.adminPath,
        routing: true,
        skipInstall: options.skipInstall,
      });
    };

    const implementAdminInterface:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const admin = new SourceQuery(tree, adminPath);
      const adminClass = strings.classify(options.name) + 'Module';

      admin.implementInterface(adminClass, 'AdminInterface', '@telavant/platform');

      return tree;
    };

    const createAdminGuard:Rule = (tree:Tree, context:SchematicContext):Rule => {
      return externalSchematic('@schematics/angular', 'guard', {
        name: options.name,
        path: options.path,
        flat: options.flat,
        project: options.project,
        skipTests: options.skipTests,
        lintFix: options.lintFix,
        spec: !options.skipTests,
        implements: [Implement.CanActivate]
      });
    };

    const setupAdminState:Rule = (tree:Tree, context:SchematicContext):Rule => {
      const statePath = buildRelativePath(normalize(`${sourceRoot}/main.ts`), adminRoot).replace(/^\.\//, '');

      return chain([
        schematic('state', {
          name: options.name,
          path: statePath,
          module: `${options.name}.module.ts`,
          skipTests: options.skipTests,
          skipInstall: options.skipInstall,
          flat: options.flat
        }),

        mergeWith(apply(url('./files/state'), [
          applyTemplates({
            ...templateOptions,
            stateClass: strings.classify(options.name) + 'State',
            modelClass: strings.classify(options.name) + 'StateModel',
          }),
          move(adminRoot),
        ]), MergeStrategy.Overwrite)
      ]);
    };

    const createDashboard:Rule = (tree:Tree, context:SchematicContext):Rule => {
      const dashboardOptions = {
        ...templateOptions,
        name: options.dashboardName || 'dashboard',
        path: adminRoot,
        inlineTemplate: false,
        inlineStyle: false,
        spec: !options.skipTests
      };

      return chain([
        externalSchematic('@schematics/angular', 'component', dashboardOptions),
        mergeWith(apply(url('./files/dashboard'), [
          applyTemplates(dashboardOptions),
          move(adminRoot)
        ]), MergeStrategy.Overwrite),
      ]);
    };

    const updateAdminModule:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const admin = new SourceQuery(tree, adminPath);

      admin.addNgImport('TelavantPlatformModule', '@telavant/platform')
        .addNgImport('FormsModule', '@angular/forms')
        .addNgImport('ReactiveFormsModule', '@angular/forms')
        .addNgImport('ClarityModule', '@clr/angular')
        .addNgImport('NgPipesModule', 'angular-pipes')
        .addNgImport('ToastrModule.forRoot()', 'ngx-toastr')
        .addNgImport('NgxUploaderModule', 'ngx-uploader')
        .addNgImport('ImageCropperModule', 'ngx-image-cropper')
        .addNgImport('QuillModule', 'ngx-quill');

      const componentPath = normalize(adminPath.replace(/\.module\.ts$/, '.component.ts'));
      const component = new SourceQuery(tree, componentPath);

      component.addImport('AdminNavigation', '@telavant/platform')
        .queryClassByDecorator('Component').queryBody()
        .prepend(`\n  navigation:AdminNavigation[] = [];`);

      return tree;
    };

    const updateAdminComponent:Rule = (tree:Tree, context:SchematicContext):Rule => {
      return chain([
        mergeWith(apply(url('./files/component'), [
          applyTemplates(templateOptions),
          move(adminRoot),
        ]), MergeStrategy.Overwrite)
      ]);
    };

    const updateAdminRouter:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const admin = new SourceQuery(tree, adminPath);
      const router = admin.queryRoutingModule();

      const guardPath  = normalize(`${adminRoot}/${options.name}.guard`);
      const guardClass = strings.classify(options.name) + 'Guard';

      const dashboardRoot  = normalize(`${adminRoot}/${options.flat ? '' : options.dashboardName}`);
      const dashboardPath  = normalize(`${dashboardRoot}/${options.dashboardName}.component`);
      const dashboardClass = strings.classify(options.dashboardName) + 'Component';

      router.addImport('DataResolver', '@telavant/platform')
        .addImport(guardClass, buildRelativePath(router.path, guardPath))
        .addImport(dashboardClass, buildRelativePath(router.path, dashboardPath));

      router.queryRoute('').query(`/SyntaxList`)
        .append(`, canActivate: [ ${guardClass} ], children: [\n  { path: '', component: ${dashboardClass} }\n]`);

      return tree;
    };

    return chain([
      addDependencies,
      setupToastr,
      setupAppState,
      createAdminModule,
      implementAdminInterface,
      createAdminGuard,
      setupAdminState,
      createDashboard,
      updateAdminModule,
      updateAdminComponent,
      updateAdminRouter
    ]);
  };
}
