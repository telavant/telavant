import { Schema as ComponentSchema } from '@schematics/angular/component/schema';

export interface Schema extends ComponentSchema {
  /**
   * The uri path to use for admin routes.
   */
  adminPath?: string;

  /**
   * The name of the dashboard component (default: 'dashboard')
   */
  dashboardName?: string;

  /**
   * When true, does not install packages for dependencies.
   */
  skipInstall?: boolean;
}
