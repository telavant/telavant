/**
 * Low level functions for manipulating the AST (abstract syntax tree).
 */

import { findNodes, insertAfterLastOccurrence } from '@schematics/angular/utility/ast-utils';
import { Change, InsertChange, NoopChange, ReplaceChange, RemoveChange } from '@schematics/angular/utility/change';
import { Identifier, Node, SourceFile, StringLiteral, SyntaxKind } from 'typescript';

/**
 * Add Export `export { symbolName } from fileName` if the import doesn't exit
 * already. Assumes fileToEdit can be resolved and accessed.
 *
 * @param source
 * @param fileToEdit (file we want to add import to)
 * @param symbolName (item to import)
 * @param fileName (path to the file)
 * @param isDefault (if true, import follows style for importing default exports)
 * @return Change
 */
export function insertExport(source:SourceFile, fileToEdit:string, symbolName:string, fileName:string, isDefault = false):Change {
  const rootNode = source;
  const allExports = findNodes(rootNode, SyntaxKind.ExportDeclaration);

  // get nodes that map to export statements from the file fileName
  const relevantExports = allExports.filter(node => {
    // StringLiteral of the ExportDeclaration is the export file (fileName in this case).
    const exportFiles = node.getChildren()
      .filter(child => child.kind === SyntaxKind.StringLiteral)
      .map(n => (n as StringLiteral).text);

    return exportFiles.filter(file => file === fileName).length === 1;
  });

  if (relevantExports.length > 0) {
    let exportsAsterisk = false;
    // exports from export file
    const exports: Node[] = [];
    relevantExports.forEach(n => {
      Array.prototype.push.apply(exports, findNodes(n, SyntaxKind.Identifier));
      if (findNodes(n, SyntaxKind.AsteriskToken).length > 0) {
        exportsAsterisk = true;
      }
    });

    // if exports * from fileName, don't add symbolName
    if (exportsAsterisk) {
      return new NoopChange();
    }

    const exportTextNodes = exports.filter(n => (n as Identifier).text === symbolName);

    // insert export if it's not there
    if (exportTextNodes.length === 0) {
      const fallbackPos =
        findNodes(relevantExports[0], SyntaxKind.CloseBraceToken)[0].getStart() ||
        findNodes(relevantExports[0], SyntaxKind.FromKeyword)[0].getStart();

      return insertAfterLastOccurrence(exports, `, ${symbolName}`, fileToEdit, fallbackPos);
    }

    return new NoopChange();
  }

  // no such export declaration exists
  const useStrict = findNodes(rootNode, SyntaxKind.StringLiteral)
    .filter((n: StringLiteral) => n.text === 'use strict');
  let fallbackPos = 0;
  if (useStrict.length > 0) {
    fallbackPos = useStrict[0].end;
  }
  const open = isDefault ? '' : '{ ';
  const close = isDefault ? '' : ' }';
  // if there are no exports or 'use strict' statement, insert export at beginning of file
  const insertAtBeginning = allExports.length === 0 && useStrict.length === 0;
  const separator = insertAtBeginning ? '' : ';\n';
  const toInsert = `${separator}export ${open}${symbolName}${close}` +
    ` from '${fileName}'${insertAtBeginning ? ';\n' : ''}`;

  if(allExports.length == 0) {
    return new InsertChange(fileToEdit, 0, toInsert);
  }

  return insertAfterLastOccurrence(
    allExports,
    toInsert,
    fileToEdit,
    fallbackPos,
    SyntaxKind.StringLiteral,
  );
}
