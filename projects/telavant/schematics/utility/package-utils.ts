import { JsonAstObject, JsonParseMode, parseJsonAst } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import {
  appendPropertyInAstObject,
  findPropertyInAstObject,
  insertPropertyInAstObjectInOrder,
} from '@schematics/angular/utility/json-utils';

const pkgJsonPath = '/package.json';

export function addPackageJsonScript(tree:Tree, name:string, script:string, overwrite:boolean = true): void {
  const packageJsonAst = _readPackageJson(tree);
  const scriptsNode = findPropertyInAstObject(packageJsonAst, 'scripts');
  const recorder = tree.beginUpdate(pkgJsonPath);

  if (!scriptsNode) {
    // Haven't found the dependencies key, add it to the root of the package.json.
    appendPropertyInAstObject(recorder, packageJsonAst, 'scripts', {
      [name]: script,
    }, 2);
  } else if (scriptsNode.kind === 'object') {
    // check if package already added
    const scriptNode = findPropertyInAstObject(scriptsNode, name);

    if (!scriptNode) {
      // Package not found, add it.
      insertPropertyInAstObjectInOrder(
        recorder,
        scriptsNode,
        name,
        script,
        4,
      );
    } else if (overwrite) {
      // Package found, update version if overwrite.
      const { end, start } = scriptNode;
      recorder.remove(start.offset, end.offset - start.offset);
      recorder.insertRight(start.offset, JSON.stringify(script));
    }
  }

  tree.commitUpdate(recorder);
}

export function addPackageJsonBrowserModule(tree:Tree, pkg:string, value:string|boolean, overwrite:boolean = true): void {
  const packageJsonAst = _readPackageJson(tree);
  const browserNode = findPropertyInAstObject(packageJsonAst, 'browser');
  const recorder = tree.beginUpdate(pkgJsonPath);

  if (!browserNode) {
    // Haven't found the dependencies key, add it to the root of the package.json.
    appendPropertyInAstObject(recorder, packageJsonAst, 'browser', {
      [pkg]: value,
    }, 2);
  } else if (browserNode.kind === 'object') {
    // check if package already added
    const packageNode = findPropertyInAstObject(browserNode, pkg);

    if (!packageNode) {
      // Package not found, add it.
      insertPropertyInAstObjectInOrder(
        recorder,
        browserNode,
        pkg,
        value,
        4,
      );
    } else if (overwrite) {
      // Package found, update version if overwrite.
      const { end, start } = packageNode;
      recorder.remove(start.offset, end.offset - start.offset);
      recorder.insertRight(start.offset, JSON.stringify(value));
    }
  }

  tree.commitUpdate(recorder);
}

function _readPackageJson(tree: Tree): JsonAstObject {
  const buffer = tree.read(pkgJsonPath);
  if (buffer === null) {
    throw new SchematicsException('Could not read package.json.');
  }
  const content = buffer.toString();

  const packageJson = parseJsonAst(content, JsonParseMode.Strict);
  if (packageJson.kind != 'object') {
    throw new SchematicsException('Invalid package.json. Was expecting an object');
  }

  return packageJson;
}
