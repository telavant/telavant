import { basename, dirname, join, normalize, Path } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import { addImportToModule, addDeclarationToModule, insertImport } from '@schematics/angular/utility/ast-utils';
import { Change, InsertChange, NoopChange, RemoveChange, ReplaceChange } from '@schematics/angular/utility/change';
import { buildRelativePath } from '@schematics/angular/utility/find-module';
import { TypescriptParser } from '@telavant/astq';
import { createSourceFile, Node, ScriptTarget, SourceFile, ScriptKind, SyntaxKind } from 'typescript';
import { insertExport } from './ast-utils';

const parser = new TypescriptParser();

export interface RouteData {
  path:string,
  pathMatch?:string,
  matcher?:string,
  component:string,
  redirectTo?:string,
  outlet?:string,
  canActivate?:string,
  canActivateChild?:string,
  canDeactivate?:string,
  canLoad?:string,
  data?:string,
  resolve?:string,
  children?:RouteData[],
  loadChildren?:string,
  runGuardsAndResolvers?:string
}

export class SourceQuery {
  readonly LocalPathRegex = new RegExp(/^\.\.?\//);

  constructor(public tree:Tree, public path:Path) {
    if(!tree.exists(path)) {
      if(tree.exists(`${path}.ts`)) {
        this.path = normalize(`${path}.ts`);
      } else {
        throw new SchematicsException(`Source file (${path}) not found`);
      }
    }
  }

  get source():SourceFile {
    const content = this.tree.read(this.path).toString('utf-8');
    return createSourceFile(this.path, content, ScriptTarget.ES2019, true, ScriptKind.TS);
  }

  query(q:string, params:any = {}):SourceQueryResult {
    return new SourceQueryResult(this.tree, this.path, q, params);
  }

  has = (q:string, params:any = {}):boolean => !this.query(q, params).empty();
  get = (q:string, params:any = {}):Node[]  => this.query(q, params).results();

  quote = (str:string, q:string = "'"):string => q + str + q;
  unquote = (str:string):string => str.replace(/(^['"`]|['"`]$)/g, '');

  /**
   * Move a Typescript file and, if necessary, rewrite local import paths based on the new location.
   */
  rename(dest:string|Path) {
    let to = normalize(dest);

    if(this.path != to) {
      if(dirname(this.path) != dirname(to)) {
        this.query(`//ImportDeclaration/StringLiteral[@literalText =~ {regex}]`, { regex: this.LocalPathRegex })
          .replace(path => ' ' + this.quote(buildRelativePath(to, join(dirname(this.path), basename(normalize(this.unquote(path)))))));
      }

      this.tree.rename(this.path, to);
      this.path = to;
    }
  }

  apply(changes:Change[]):number {
    //changes.filter(change => change.path == '/src/app/app.module.ts').forEach(change => console.log(change));
    let count = 0;

    changes
      .filter(change => !(change instanceof NoopChange))
      .map(change => change.path)
      .reduce((paths, path) => paths.includes(path) ? paths : [...paths, path], [])
      .forEach(path => {
        const recorder = this.tree.beginUpdate(path);

        changes.filter(change => change.path == path).forEach(change => {
          if(change instanceof InsertChange) {
            recorder.insertLeft(change.pos, change.toAdd);
          } else if(change instanceof ReplaceChange) {
            let replaceChange = change as any; // access private properties
            recorder.remove(replaceChange.pos, replaceChange.oldText.length);
            recorder.insertLeft(replaceChange.pos, replaceChange.newText);
          } else if(change instanceof RemoveChange) {
            let removeChange = change as any; // access private properties
            recorder.remove(removeChange.pos, removeChange.toRemove.length);
          }

          count++;
        });

        this.tree.commitUpdate(recorder);
      });

    return count;
  }

  isLocalPath      = (path:string):boolean => path.match(this.LocalPathRegex) != null;
  resolveLocalPath = (path:string):Path    => normalize(`${dirname(this.path)}/${path}.ts`);

  /**
   * Returns the current identifier for the specified import.
   */
  getIdentifier = (name:string, path:string):string => this.query(`
    //ImportDeclaration[/StringLiteral[@literalText == {path}]]
    //ImportSpecifier[@name == {name}]/Identifier[last()]
  `, { name, path }).text();

  /**
   * Returns the resolved file path of the specified import.
   */
  getImportPath = (name:string, onlyLocals:boolean = false):string =>
    this.get(`//ImportDeclaration[//ImportSpecifier[@name == {name}]]/StringLiteral`, { name })
      .map(node => this.unquote(node.getText()))
      .filter(file => onlyLocals ? this.isLocalPath(file) : true)
      .pop();

  /**
   * Returns all import declarations matching the provided name and path.
   */
  queryImported = (name:string, path?:string):SourceQueryResult => this.query(!path
    ? `//ImportDeclaration[//ImportSpecifier[@name == {name}]]`
    : `//ImportDeclaration[//ImportSpecifier[@name == {name}] && /StringLiteral[@literalText == {path}]]`,
    { name, path }
  );

  /**
   * Returns whether a symbol has been imported from the specified path.
   */
  isImported = (name:string, path?:string):boolean => !this.queryImported(name, path).empty();

  /**
   * Creates an import if it doesn't already exist.
   */
  addImport(name:string, path:string):this {
    if(!this.isImported(name, path)) {
      this.apply([insertImport(this.source, this.path, name, path)]);
    }

    return this;
  }

  /**
   * Returns all export declarations matching the provided name and path.
   */
  queryExported = (name:string, path?:string):SourceQueryResult => this.query(!path
    ? `//ExportDeclaration[//ExportSpecifier[@name == {name}]]`
    : `//ExportDeclaration[//ExportSpecifier[@name == {name}] && /StringLiteral[@literalText == {path}]]`,
    { name, path }
  );

  /**
   * Returns whether a symbol has been exported from the specified path.
   */
  isExported = (name:string, path?:string):boolean => !this.queryExported(name, path).empty();

  /**
   * Creates an export if it doesn't already exist.
   */
  addExport(name:string, path:string, isDefault = false):this {
    if(!this.isExported(name, path)) {
      this.apply([insertExport(this.source, this.path, name, path, isDefault)]);
    }

    return this;
  }

  /**
   * Returns a matching PropertyAssignment from the specified decorator.
   */
  queryDecoratorProperty = (property:string) => this.query(`
    /SyntaxList/Decorator//PropertyAssignment[@name == {property}]
  `, { property });

  /**
   * Returns a matching PropertyAssignment from the specified decorator.
   */
  queryDecoratorType = () => this.query(`
    /SyntaxList/Decorator//TypeReference/Identifier
  `);

  /**
   * Queries the specified @NgModule property.
   */
  queryNgModuleProperty = (prop:string):SourceQueryResult => this.queryClassByDecorator('NgModule')
    .queryDecoratorProperty(prop)
    .query(`/ArrayLiteralExpression`);

  getNgImportModules = ():string[] => this.queryNgModuleProperty('imports').query(`
    //Identifier[../ArrayLiteralExpression || (../PropertyAccessExpression && first())]
  `).results().map(node => node.getText());

  /**
   * Adds the specified class to the NgModule declarations list and imports the module.
   */
  addNgDeclaration = (symbol:string, importPath:string):this => {
    this.apply(addDeclarationToModule(this.source, this.path, symbol, importPath));
    return this;
  };

  /**
   * Adds the specified class to the NgModule imports list and imports the module.
   */
  addNgImport = (symbol:string, importPath:string):this => {
    this.apply(addImportToModule(this.source, this.path, symbol, importPath));
    return this;
  };

  queryClass = (name:string):SourceQueryResult =>
    this.query(`//ClassDeclaration[@name == {name}]`, { name });

  queryClassByDecorator = (name:string):SourceQueryResult =>
    this.query(`//ClassDeclaration[/SyntaxList/Decorator[@name == {name}]]`, { name });

  queryClassByInterface = (name:string):SourceQueryResult =>
    this.query(`//ClassDeclaration[//HeritageClause[@name == {name}]]`, { name });

  getDecoratedClassName = (name:string):string => this.queryClassByDecorator(name).query("/Identifier").text();

  queryMethodByDecorator = (decorator:string):SourceQueryResult => this.query(`
    //MethodDeclaration[/SyntaxList/Decorator[@name == {decorator}]]
  `, { decorator });

  /**
   * Updates the source add an interface to the specified class's implements list.
   */
  implementInterface(className:string, interfaceName:string, importPath:string):this {
    const classDeclaration = this.queryClass(className);

    if(classDeclaration.empty()) {
      throw new SchematicsException(`Unable to locate a class named ${className}`)
    }

    if(classDeclaration.query(`[//HeritageClause[@name == {interfaceName}]]`, { interfaceName })) {
      // try updating an existing "implements" clause
      if(!classDeclaration.query(`//HeritageClause`).append(`, ${interfaceName}`)) {
        // no "implements" clause found, append a new one to the class identifier
        if(!classDeclaration.query('/Identifier').append(` implements ${interfaceName}`)) {
          throw new SchematicsException(`Failed to implement ${interfaceName} on ${className}.`);
        }
      }

      this.addImport(interfaceName, importPath);
    }

    return this;
  }

  queryInterface = (name:string):SourceQueryResult =>
    this.query(`//InterfaceDeclaration[@name == {name}]`, { name });

  queryBody = ():SourceQueryResult =>
    this.query(`/OpenBraceToken+/SyntaxList`);

  /**
   * Returns true if this source file is an Angular routing module.
   */
  isRoutingModule():boolean {
    const RouterModule = this.getIdentifier('RouterModule', '@angular/router');

    return !!RouterModule && !this.queryNgModuleProperty('imports').query(`
      //CallExpression/PropertyAccessExpression/Identifier[@text == {RouterModule}]
    `, { RouterModule }).empty();
  }

  queryRoutingModule = ():SourceQuery => this.getNgImportModules()
    .map(name    => this.getImportPath(name))
    .filter(path => this.isLocalPath(path))
    .map(path    => this.resolveLocalPath(path))
    .map(path    => new SourceQuery(this.tree, path))
    .filter(src  => src.isRoutingModule())
    .pop() || null;

  queryRoutes():SourceQueryResult {
    const RouterModule = this.getIdentifier('RouterModule', '@angular/router');

    const value = this.queryNgModuleProperty('imports').query(`
      //CallExpression[/PropertyAccessExpression[@text =~ {router} ]]
      /SyntaxList/*[./Identifier || ./ArrayLiteralExpression]
    `, { router: `^${RouterModule}\.for` }).resolve();

    if(value.empty()) {
      throw new SchematicsException(`Cannot find any routes in ${this.path}.`);
    }

    return value;
  }

  queryRoute = (path:string):SourceQueryResult => this.query(`
    //ObjectLiteralExpression[//PropertyAssignment[
      @name == 'path' && /StringLiteral[${path.length > 0 ? '@literalText == {path}' : '!@literalText'}]
    ]]
  `, { path });

  queryChildRoutes = () => this.query(`/SyntaxList/PropertyAssignment[@name == 'children']/ArrayLiteralExpression`).resolve();

  compileRoute(route:RouteData, level = 1):string {
    const quoted = [ 'path', 'pathMatch', 'redirectTo', 'outlet' ];
    const indent = '\n' + '  '.repeat(level);

    const properties = Object.entries(route)
      .map(([key, val]) => [key, typeof val == 'string' ? val :
        '[' + val.map(child => this.compileRoute(child, level + 1)).join(',') + indent + ']'])
      .map(([key, val]) => key + ': ' + (quoted.includes(key) ? this.quote(val) : val));

    return `${indent}{ ${properties.join(', ')} }`;
  }

  addRoute(route:RouteData, parent:string = null):this {
    const error = `Failed to add route path '${route.path}' for ${route.component} to ${this.path}`;
    let routes = this instanceof SourceQueryResult ? this : this.queryRoutes();
    let level = 1;

    if(typeof parent == 'string') {
      level = routes.query(`
        //ObjectLiteralExpression[//PropertyAssignment[
          @name == 'path' && /StringLiteral[${parent.length > 0 ? '@literalText == {parent}' : '!@literalText'}]
        ]]
      `, { parent }).count() + 1;

      routes = routes.queryRoute(parent).queryChildRoutes();
    }

    if(!routes.queryRoute(route.path).empty()) {
      throw new SchematicsException(`${error}: route path '${route.path}' already exists.`);
    }

    const list  = routes.query('/SyntaxList');
    const comma = list.empty() || list.text().match(/(^|,)$/) ? '' : ',';
    const code  = comma + this.compileRoute(route, level);

    if(!list.append(code)) {
      throw new SchematicsException(`${error}.`);
    }

    return this;
  }

  queryStateModule():SourceQuery {
    if(!this.path.match(/\.module\.ts$/)) {
      return null;
    }

    try {
      return new SourceQuery(this.tree, normalize(this.path.replace(/\.module\.ts$/, '.state.ts')));
    } catch(err) {
      return null;
    }
  }
}

export class SourceQueryResult extends SourceQuery {
  constructor(tree:Tree, path:Path, public baseQuery:string = '', public params:any = {}) {
    super(tree, path);
  }

  query(q:string, params:any = {}):SourceQueryResult {
    return super.query(this.baseQuery + q, { ...this.params, ...params });
  }

  results = ():Node[] => {
    return parser.parse(this.source, this.baseQuery, this.params);
  };

  resolve():SourceQueryResult {
    switch(this.kind()) {
      case SyntaxKind.Identifier:
      case SyntaxKind.BarEqualsToken:
        const name = this.text();
        const declaration = `//VariableDeclaration[@name == {name}]/*[last()]`;
        let value = this.root().query(declaration, { name }).resolve();

        if(value.empty()) {
          const importPath = this.getImportPath(this.text(), true);
          const external = importPath ? new SourceQuery(this.tree, this.resolveLocalPath(importPath)) : null;
          value = external ? external.query(declaration, { name }).resolve() : value;
        }

        return value;

      case SyntaxKind.PropertyAssignment:
        return this.query(`/*[last()]`).resolve();

      default:
        return this;
    }
  };

  count = ():number            => this.results().length;
  empty = ():boolean           => this.count() == 0;
  nth   = (i:number):Node      => this.empty() ? null : this.results()[i > 0 ? i - 1 : this.count() + i + 1];
  first = ():Node              => this.nth(1);
  last  = ():Node              => this.nth(-1);
  text  = ():string            => this.empty() ? null : this.first().getText();
  kind  = ():SyntaxKind        => this.empty() ? null : this.first().kind;
  root  = ():SourceQuery       => new SourceQuery(this.tree, this.path);
  and   = ():SourceQueryResult => this.query(`../*`);

  debug(msg?:string):this {
    let query = this.baseQuery.trim().replace(/\n\s+/mg, '\n  ');

    Object.entries(this.params).forEach(([key, value]) => {
      query = query.replace(`{${key}}`, typeof value == 'string' ? this.quote(value) : (value ? value.toString() : null))
    });

    console.log(`${msg ? msg + '\n' : ''}[${this.path}]\n${query}\n`);

    this.results().map(node => parser.output(node));
    return this;
  };

  replace = (arg:string|Function):number => this.apply(this.results().map(
    node => new ReplaceChange(this.path, node.getFullStart(), node.getFullText(),
      typeof arg == 'string' ? arg : arg(node.getFullText()))
  ));

  prepend = (content:string):number => this.apply(this.results().map(
    node => new InsertChange(this.path, node.getFullStart(), content)
  ));

  append = (content:string):number => this.apply(this.results().map(
    node => new InsertChange(this.path, node.getFullStart() + node.getFullWidth(), content)
  ));

  delete = ():number => this.apply(this.results().map(
    node => new RemoveChange(this.path, node.getFullStart(), node.getFullText())
  ));
}
