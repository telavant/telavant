import { JsonAstObject, JsonAstArray, JsonValue } from '@angular-devkit/core';
import { UpdateRecorder } from '@angular-devkit/schematics';

export function appendPropertyInAstObject(recorder:UpdateRecorder, node:JsonAstObject, propertyName:string, value:JsonValue, indent = 4) {
  const indentStr = '\n' + new Array(indent + 1).join(' ');

  if (node.properties.length > 0) {
    // Insert comma.
    const last = node.properties[node.properties.length - 1];
    recorder.insertRight(last.start.offset + last.text.replace(/\s+$/, '').length, ',');
  }

  recorder.insertLeft(
    node.end.offset - 1,
    '  '
    + `"${propertyName}": ${JSON.stringify(value, null, 2).replace(/\n/g, indentStr)}`
    + indentStr.slice(0, -2),
  );
}

export function updatePropertyInAstObject(recorder:UpdateRecorder, node:JsonAstObject, propertyName:string, value:JsonValue, indent = 4) {
  const indentStr = '\n' + new Array(indent + 1).join(' ');

  // Find the property inside the object.
  const propIdx = node.properties.findIndex(prop => prop.key.value === propertyName);

  if (propIdx === -1) {
    // There's nothing to update.
    return;
  }

  const propValue = node.properties[propIdx].value;
  recorder.remove(propValue.start.offset, propValue.end.offset - propValue.start.offset);
  recorder.insertRight(propValue.start.offset, JSON.stringify(value, null, 2).replace(/\n/g, indentStr));
}

export function removePropertyInAstObject(recorder:UpdateRecorder, node:JsonAstObject, propertyName:string) {
  // Find the property inside the object.
  const propIdx = node.properties.findIndex(prop => prop.key.value === propertyName);

  if (propIdx === -1) {
    // There's nothing to remove.
    return;
  }

  if (node.properties.length === 1) {
    // This is a special case. Everything should be removed, including indentation.
    recorder.remove(node.start.offset, node.end.offset - node.start.offset);
    recorder.insertRight(node.start.offset, '{}');

    return;
  }

  // The AST considers commas and indentation to be part of the preceding property.
  // To get around messy comma and identation management, we can work over the range between
  // two properties instead.
  const previousProp = node.properties[propIdx - 1];
  const targetProp = node.properties[propIdx];
  const nextProp = node.properties[propIdx + 1];

  let start, end;
  if (previousProp) {
    // Given the object below, and intending to remove the `m` property:
    // "{\n  \"a\": \"a\",\n  \"m\": \"m\",\n  \"z\": \"z\"\n}"
    //                        ^---------------^
    // Removing the range above results in:
    // "{\n  \"a\": \"a\",\n  \"z\": \"z\"\n}"
    start = previousProp.end;
    end = targetProp.end;
  } else {
    // If there's no previousProp there is a nextProp, since we've specialcased the 1 length case.
    // Given the object below, and intending to remove the `a` property:
    // "{\n  \"a\": \"a\",\n  \"m\": \"m\",\n  \"z\": \"z\"\n}"
    //       ^---------------^
    // Removing the range above results in:
    // "{\n  \"m\": \"m\",\n  \"z\": \"z\"\n}"
    start = targetProp.start;
    end = nextProp.start;
  }

  recorder.remove(start.offset, end.offset - start.offset);
  if (!nextProp) {

    recorder.insertRight(start.offset, '\n');
  }
}

function _buildIndent(count: number): string {
  return count ? '\n' + ' '.repeat(count) : '';
}

export function appendValuesInAstArray(recorder:UpdateRecorder, node:JsonAstArray, values:JsonValue[], indent = 4) {
  let index = node.start.offset + 1;
  let content = '';

  if (node.elements.length > 0) {
    // Insert comma.
    const last = node.elements[node.elements.length - 1];
    recorder.insertRight(last.end.offset, ',');
    index = indent ? last.end.offset + 1 : last.end.offset;
  }

  values.forEach((value, i) => {
    let indentStr = _buildIndent(indent);

    if(i < values.length - 1) {
      indentStr = ',' + indentStr;
    }

    content += (node.elements.length === 0 && indent ? '\n' : '')
      + ' '.repeat(indent)
      + JSON.stringify(value, null, indent).replace(/\n/g, indentStr)
      + indentStr.slice(0, -indent);
  });

  recorder.insertRight(index, content);
}
