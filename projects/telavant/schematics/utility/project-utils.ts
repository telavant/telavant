/**
 * Project related utility functions.
 */

import { Rule, Tree, DirEntry, FileVisitorCancelToken, SchematicsException } from '@angular-devkit/schematics';
import { normalize, dirname, basename, strings, Path } from '@angular-devkit/core';
import { getProjectTargets, targetBuildNotFoundError } from '@schematics/angular/utility/project-targets';
import { WorkspaceProject, WorkspaceTargets } from '@schematics/angular/utility/workspace-models';
import { findBootstrapModulePath } from '@schematics/angular/utility/ng-ast-utils';
import { findModuleFromOptions, ModuleOptions } from '@schematics/angular/utility/find-module';
import { SourceQuery } from './source-query';

export function getTarget(project:WorkspaceProject, name:string) {
  const targets:WorkspaceTargets = getProjectTargets(project);

  if(!(name in targets)) {
    if(name == 'build') {
      throw targetBuildNotFoundError();
    }

    return null;
  }

  return targets[name];
}

export function getAppPath(tree:Tree, project:WorkspaceProject):Path {
  const target = getTarget(project, 'build');
  const relativePath = findBootstrapModulePath(tree, target.options.main);

  return normalize(`/${project.sourceRoot}/${relativePath}.ts`);
}

export function getAdminPath(tree:Tree, project:WorkspaceProject, options:ModuleOptions = null):Path {
  let path:Path = null;

  if(options) {
    path = findModuleFromOptions(tree, options);
  }

  if(!path) {
    let dir:DirEntry = tree.getDir('/' + project.sourceRoot);

    try {
      dir.visit(file => {
        if(file.match(/\.module\.ts$/)) {
          let source = new SourceQuery(tree, file);

          if(!source.queryClassByInterface('AdminInterface').empty()) {
            path = file;
            throw FileVisitorCancelToken;
          }
        }
      });
    } catch(err) {
      if(err != FileVisitorCancelToken) {
        throw err;
      }
    }
  }

  if(!path) {
    throw new SchematicsException("Unable to locate admin module.");
  }

  return path;
}

export function getServerPath(tree:Tree, arg:string|WorkspaceProject):Path {
  let tsconfigPath:string;

  if(typeof arg == 'string') {
    tsconfigPath = arg;
  } else {
    const target = getTarget(arg, 'server');

    if(!target) {
      return null;
    }

    tsconfigPath = target.options.tsConfig;
  }

  const tsconfig = JSON.parse(tree.read(tsconfigPath).toString());
  const tsconfigDir = dirname(normalize('/' + tsconfigPath));
  const [ modulePath ] = tsconfig.angularCompilerOptions.entryModule.split('#');

  return normalize(`${tsconfigDir}/${modulePath}.ts`);
}
