import { Schema } from '../schema';

export interface SqliteSchema extends Schema {
  /**
   * The type of database to configure.
   */
  type:'sqlite';

  /**
   * The path to the database file.
   */
  database:string;
}