import { Schema } from '../schema';

export interface MysqlSchema extends Schema {
  /**
   * The type of database to configure.
   */
  type:'mysql';

  /**
   * The hostname of the MySQL server.
   */
  host:string;

  /**
   * The MySQL port number to connect to.
   */
  port:number;

  /**
   * The name of the MySQL database to use.
   */
  database:string;

  /**
   * The MySQL username to login with.
   */
  username:string;

  /**
   * The MySQL password to login with.
   */
  password:string;
}