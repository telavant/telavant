import { Rule, Tree, SchematicContext, chain } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { addPackageJsonDependency, NodeDependencyType } from '@schematics/angular/utility/dependencies';
import { Schema } from './schema';

const CONFIG_FILE = 'ormconfig.json';

const DEFAULT_CONFIG = {
  mysql: {
    name: 'default',
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: '',
    password: '',
    database: '',
    synchronize: true
  },

  sqlite: {
    name: 'default',
    type: 'sqlite',
    database: 'database.sql',
    synchronize: true
  }
};

export default function(options:Schema):Rule {
  const addDependencies = (host:Tree, context:SchematicContext):Tree => {
    switch(options.type) {
      case 'mysql':
        addPackageJsonDependency(host, { name: 'mysql', version: '^2.17.1', type: NodeDependencyType.Default });
        break;

      case 'sqlite':
        addPackageJsonDependency(host, { name: 'sqlite3', version: '^4.0.7', type: NodeDependencyType.Default });
        break;
    }

    if(!options.skipInstall) {
      context.addTask(new NodePackageInstallTask());
    }

    return host;
  };

  const updateORMConfiguration = (host:Tree, context:SchematicContext):Tree => {
    const configuration = { ...DEFAULT_CONFIG[options.type], ...options };
    let configs = host.exists(CONFIG_FILE) ? JSON.parse(host.read(CONFIG_FILE).toString('utf-8')) : [];

    if(!Array.isArray(configs)) {
      configs = [configs];
    }

    // remove empty default configuration
    configs = configs.filter(config => 'name' in config);

    const i = configs.findIndex(config => config.name == 'default');

    if(i > -1) {
      configs[i] = configuration;
    } else {
      configs.push(configuration);
    }

    const content = JSON.stringify(configs, null, 2);

    if(host.exists(CONFIG_FILE)) {
      host.overwrite(CONFIG_FILE, content);
    } else {
      host.create(CONFIG_FILE, content);
    }

    return host;
  };

  return (host:Tree, context:SchematicContext):Rule => {
    return chain([
      addDependencies,
      updateORMConfiguration
    ]);
  };
}
