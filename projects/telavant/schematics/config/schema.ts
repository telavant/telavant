export interface Schema {
  /**
   * The type of database to configure.
   */
  type:'mysql' | 'sqlite';

  /**
   * When true, does not install packages for dependencies.
   */
  skipInstall?: boolean;
}
