import { Rule, Tree, SchematicContext, chain, externalSchematic, apply, applyTemplates, MergeStrategy, mergeWith, move, url } from '@angular-devkit/schematics';
import { normalize, strings } from '@angular-devkit/core';
import { getWorkspace } from '@schematics/angular/utility/config';
import { buildRelativePath } from '@schematics/angular/utility/find-module';
import { buildDefaultPath, getProject } from '@schematics/angular/utility/project';
import { getAppPath, SourceQuery } from '../utility';
import { Schema } from './schema';

export default function(options:Schema):Rule {
  return (host:Tree, context:SchematicContext):Rule => {
    const workspace = getWorkspace(host);
    options.project = options.project || workspace.defaultProject;
    const project   = getProject(host, options.project);

    options = {
      ...options,
      ...project.schematics['@schematics/angular:component'],
      path: options.path || buildDefaultPath(project),
    };

    const appPath   = getAppPath(host, project);
    const name      = strings.dasherize(options.name);

    const vars = {
      ...strings,
      ...options,
      notFlat: (s: string) => options.flat ? '' : s,
      baseName: name,
      componentClass: strings.classify(options.name) + 'Component',
    };

    const installTemplates:Rule = (tree:Tree, context:SchematicContext):Rule => mergeWith(apply(url('./files'), [
      applyTemplates(vars),
      move(options.path),
    ]), MergeStrategy.Overwrite);

    const setupComponent:Rule = (tree:Tree, context:SchematicContext):Tree => {
      const app = new SourceQuery(tree, appPath);
      const componentPath = normalize(`${options.path}/${vars.notFlat(options.name)}/${name}.component`);

      app.addNgDeclaration(vars.componentClass, buildRelativePath(app.path, componentPath));

      const router = app.queryRoutingModule();

      router.addImport(vars.componentClass, buildRelativePath(router.path, componentPath));

      router.queryRoutes()
        .addRoute({ path: name, component: vars.componentClass }, '');

      return tree;
    };

    return chain([
      externalSchematic('@schematics/angular', 'component', options),
      installTemplates,
      setupComponent
    ]);
  };
}
