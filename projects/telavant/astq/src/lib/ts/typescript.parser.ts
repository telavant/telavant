import ASTQ from 'astq';
import { Node, SourceFile, SyntaxKind } from 'typescript';
import { TypescriptAdapter } from './typescript.adapter';
import { getSyntaxKind } from './typescript.utils';

export class TypescriptParser {
  private astq:ASTQ<Node>;

  constructor() {
    this.astq = new ASTQ<Node>();
    this.astq.adapter(new TypescriptAdapter(this.astq));
  }

  parse(source:SourceFile|Node, query?:string, params?:any):Node[] {
    return !!query ? this.astq.query(source, query, params) : source.getChildren();
  }

  output(node:Node|Node[], level:number = 0) {
    const indentWidth = 2;

    if(Array.isArray(node)) {
      node.forEach(child => this.output(child));
    } else {
      const indent = ' '.repeat(level * indentWidth);
      const text = (node.getChildCount() == 0 && node.kind != SyntaxKind.EndOfFileToken ? ': ' + node.getText() : '');

      console.log(indent + getSyntaxKind(node) + text);

      node.getChildren().forEach(child => this.output(child, level + 1));

      if(!level) {
        console.log('');
      }
    }
  }
}
