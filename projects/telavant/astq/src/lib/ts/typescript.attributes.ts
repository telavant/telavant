import { Node, isIdentifier, isSourceFile, isStringLiteral } from 'typescript';
import { findNode, getSyntaxKind, isType, isVariable } from './typescript.utils';
import { AstAttributes } from '../ast.types';

export const TypescriptAttributes:AstAttributes<Node> = {
  name: (node:Node):string => {
    if(isSourceFile(node)) {
      return node.fileName.split(/[\\/]/).pop();
    }

    const id = findNode(node, isIdentifier);
    return id ? id.getText() : undefined;
  },

  type: (node:Node):string => {
    if(isType(node)) {
      return node.getText();
    }

    const children = isVariable(node) ? node.getChildren() : node.parent.getChildren();
    const id = children.find(n => isType(n));
    return id ? id.getText() : undefined;
  },

  kind:        (node:Node):string => getSyntaxKind(node),
  text:        (node:Node):string => node.getText(),
  literalText: (node:Node):string => isStringLiteral(node) ? node['text'] : undefined,
};
