import ASTQ, { ASTQAdapter } from 'astq';
import { Node } from 'typescript';
import { TypescriptAttributes } from './typescript.attributes';
import { TypescriptFunctions } from './typescript.functions';
import { getChildren, getParent, getSyntaxKind, isNode } from './typescript.utils';

export class TypescriptAdapter implements ASTQAdapter<Node> {
  constructor(astq:ASTQ<Node>) {
    Object.entries(TypescriptFunctions).forEach(([name, callback]) => astq.func(name, callback));
  }

  taste            = (node:Node):boolean      => isNode(node);
  getParentNode    = (node:Node):Node         => getParent(node);
  getChildNodes    = (node:Node):Node[]       => getChildren(node);
  getNodeType      = (node:Node):string       => getSyntaxKind(node);
  getNodeAttrNames = (node:Node):string[]     => Object.keys(TypescriptAttributes);
  getNodeAttrValue = (node:Node, attr:string) => attr in TypescriptAttributes ? TypescriptAttributes[attr](node) : null;
}
