import { ASTQAdapter } from 'astq';
import { Node } from 'typescript';
import { AstFunctions } from '../ast.types';

export const TypescriptFunctions:AstFunctions<Node> = {
  name:        (adapter:ASTQAdapter<Node>, node:Node) => adapter.getNodeAttrValue(node, 'name'),
  type:        (adapter:ASTQAdapter<Node>, node:Node) => adapter.getNodeAttrValue(node, 'type'),
  kind:        (adapter:ASTQAdapter<Node>, node:Node) => adapter.getNodeAttrValue(node, 'kind'),
  text:        (adapter:ASTQAdapter<Node>, node:Node) => adapter.getNodeAttrValue(node, 'text'),
  literalText: (adapter:ASTQAdapter<Node>, node:Node) => adapter.getNodeAttrValue(node, 'literalText'),
  debug:       (adapter:ASTQAdapter<Node>, node:Node) => console.debug(node),
};
