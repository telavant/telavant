import { Node, SyntaxKind, isClassDeclaration, isPropertyAssignment, isVariableDeclaration, isArrayTypeNode, isTypeReferenceNode } from 'typescript';

export function isNode(node:Node):boolean {
  return !!node
    && typeof node == 'object'
    && 'kind'   in node
    && 'flags'  in node
    && 'parent' in node;
}

export function isVariable(node:Node):boolean {
  return isNode(node) && (isVariableDeclaration(node) || isPropertyAssignment(node));
}

export function isType(node:Node):boolean {
  return isNode(node) && (isTypeReferenceNode(node) || isArrayTypeNode(node));
}

export function getSyntaxKind(node:Node):string {
  if(!isNode(node)) {
    return 'undefined';
  }

  return Object.keys(SyntaxKind)
    .filter(function(key) { return isNaN(parseInt(key, 10)) })
    .filter(function(name) { return parseInt(SyntaxKind[name], 10) == node.kind })
    .shift() ?? 'undefined';
}

export function getParent(node:Node):Node {
  return isNode(node) && node.parent !== node ? node.parent : null;
}

export function getChildren(node:Node):Node[] {
  return isNode(node) ? node.getChildren() : [];
}

export function findNode(node:Node, check:(node:Node) => boolean) {
  if(check(node)) {
    return node;
  }

  const children:Node[] = node.getChildren();
  let id:Node = children.find(n => check(n));

  if(id) {
    return id;
  }

  for(let child of children) {
    id = findNode(child, check);

    if(id) {
      return id;
    }
  }

  return null;
}
