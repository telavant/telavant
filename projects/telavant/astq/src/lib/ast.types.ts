import { ASTQAdapter } from 'astq';

export declare type AstAttribute<T>  = (n:T) => string;
export declare type AstAttributes<T> = { [k:string]:AstAttribute<T> }

export declare type AstFunction<T>  = (adapter:ASTQAdapter<T>, node:T, ...args:any[]) => any;
export declare type AstFunctions<T> = { [k:string]:AstFunction<T> }
