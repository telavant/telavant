#!/usr/bin/env node
'use strict';

const [,, file, query] = process.argv;
const astq = require('@telavant/astq');
const fs = require('fs');
const ts = require('typescript');
const parser = new astq.TypescriptParser();
const content = fs.readFileSync(file).toString('utf-8');
const source = ts.createSourceFile(file, content, ts.ScriptTarget.ES2019, true, ts.ScriptKind.TS);

if(query) {
  console.log("[ASTQ]", query, '\n');
}

parser.output(parser.parse(source, query));
