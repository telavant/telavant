import { Component, OnInit } from '@angular/core';
import { MailService, RequestService, ResponseService } from '@telavant/platform';

@Component({ selector: 'ng-backend-data', template: '{{ result }}' })
export class MailBackend implements OnInit {
  result:string;

  constructor(protected request:RequestService, protected response:ResponseService, protected mail:MailService) {}

  async ngOnInit() {
    // if(this.request.getMethod() != 'POST') {
    //   this.response.setNotFound();
    //   return;
    // }

    try {
      this.result = JSON.stringify({
        type:    'success',
        message: "The message has been sent.",
        data:    await this.mail.sendMail(this.request.getBody())
      })
    } catch(err) {
      this.result = JSON.stringify({
        type:    'error',
        message: `Error sending message: ${err.reason}`,
        data:    err
      })
    }
  }
}
