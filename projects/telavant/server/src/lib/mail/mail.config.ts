import { FactoryProvider } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { MailConfig } from '@telavant/platform';
import { readFileSync } from 'fs';

export declare type MailProfile = { name: string, email: string };
export declare type MailAddresses = { [id:string]: MailProfile | string }

export class MailServerConfig extends MailConfig {
  addresses:MailAddresses;

  options: {
    host: string;
    port: number;
    secure: boolean;

    auth?: {
      user: string;
      pass: string;
    }
  };
}

export function loadMailConfig(state:TransferState):MailServerConfig {
  const config = new MailServerConfig();
  Object.assign(config, JSON.parse(readFileSync('conf/smtp.json').toString('utf-8')));

  // create browser config by excluding server-only properties
  const { options, addresses, ...browserConfig } = config;

  const stateKey = makeStateKey<MailConfig>('MailConfig');
  state.set<MailConfig>(stateKey, browserConfig as MailConfig);

  return config;
}

export const MailServerConfigProvider:FactoryProvider = {
  provide: MailConfig,
  useFactory: loadMailConfig,
  deps: [ TransferState ],
};

