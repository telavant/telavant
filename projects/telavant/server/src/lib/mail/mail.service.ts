import { ClassProvider, Inject, Injectable } from '@angular/core';
import { MailMessage, MacroTaskWrapperService, MailService, MailConfig, MailResponse } from '@telavant/platform';
import { createTransport, SendMailOptions } from 'nodemailer';
import SMTPConnection from 'nodemailer/lib/smtp-connection';
import { MailServerConfig, MailProfile } from './mail.config';

@Injectable()
export class ServerMailService {
  constructor(@Inject(MailConfig) private config:MailServerConfig, public macrotasks:MacroTaskWrapperService) { }

  getAddress(id:string):string {
    if(typeof this.config.addresses != 'object') {
      return null;
    }

    if(id in this.config.addresses && typeof this.config.addresses[id] == 'string') {
      return this.config.addresses[id] as string;
    }

    let profile:MailProfile | string = id in this.config.addresses ? this.config.addresses[id] :
      Object.values(this.config.addresses).find(address => address == id || typeof address == 'object' && address.email == id);

    if(typeof profile == 'object') {
      let address = profile.email;

      if(typeof profile.name == 'string' && profile.name.length > 0) {
        address = `${profile.name} <${address}>`;
      }

      return address;
    }

    return profile;
  }

  async sendMail(message:MailMessage):Promise<MailResponse> {
    // extract body and headers from the message object
    const { body, ...headers } = message;

    // FIXME: replace with real input sanitization
    Object.entries(headers).forEach(([key, value]) => {
      if(value.includes('\n')) {
        throw new Error(`MailService.sendMail(): failed sending mail: input for message.${key} contains forbidden characters.`);
      }
    });

    if(!headers.from || !headers.from.length) {
      headers.from = 'default';
    }

    const sender    = this.getAddress(headers.from || 'default');
    const recipient = this.getAddress(headers.to   || 'default');

    if(!sender && !recipient) {
      console.error(`MailService.sendMail(): failed sending mail: either the From: or To: address must be listed in MailConfig.addresses.`);
      throw new Error("Failed sending mail: invalid sender or recipient.");
    }

    return await this.macrotasks.wrapMacroTask<MailResponse>('sendMail', this.send({
      from:    sender || headers.from,
      to:      recipient || headers.to,
      subject: headers.subject,
      text:    body,
      html:    body
    }));
  }

  private async send(mail:SendMailOptions):Promise<MailResponse> {
    try {
      const transport = createTransport(this.config.options as SMTPConnection.Options);
      await transport.verify();
      return await transport.sendMail(mail);
    } catch(err) {
      console.error(`[MailService] Error sending message:`, err);
      console.error(`[MailService] Message data:`, mail);
      throw err;
    }
  }
}

export const MailServerServiceProvider:ClassProvider = { provide: MailService, useClass: ServerMailService };
