import { NgModule } from '@angular/core';
import { MailBackend } from './mail.backend';
import { MailServerConfigProvider } from './mail.config';
import { MailServerServiceProvider } from './mail.service';

@NgModule({
  declarations: [ MailBackend ],
  imports: [ ],
  exports: [ MailBackend ],
  providers: [
    MailServerConfigProvider,
    MailServerServiceProvider
  ]
})
export class MailServerModule {
}
