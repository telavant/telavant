import { Connection, ConnectionOptions, createConnection, getConnection } from 'typeorm';
import { readFileSync } from 'fs';

export class TypeormServer {
  config:ConnectionOptions;
  connection:Connection;

  constructor(name:string = 'default', file:string = 'conf/ormconfig.json') {
    let configs = JSON.parse(readFileSync(file).toString('utf-8'));

    if(!Array.isArray(configs)) {
      configs = [ configs ];
    }

    this.config = configs.length == 1 ? configs.pop() : configs.find(cfg => cfg.name == name);
  }

  async connect(config?:ConnectionOptions) {
    config = { ...this.config, ...config } as any;

    try {
      this.connection = getConnection(config.name);
      return;
    } catch(err) {
      // not connected
    }

    try {
      this.connection = await createConnection(config);

      if(config.synchronize) {
        await this.connection.synchronize();
      }

      console.log(`Connected to ${this.connection.options.type} database '${this.connection.options.database}'`);
    } catch(err) {
      console.error(`Database connection failed: ${err.message}`);
    }
  }
}
