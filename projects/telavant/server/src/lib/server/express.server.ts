import 'reflect-metadata';
import 'zone.js/dist/zone-node';
import bodyParser from 'body-parser';
import chokidar from 'chokidar';
import cors from 'cors';
import compression from 'compression';
import express, { Request, Response, Application as ExpressApplication } from 'express';
import { RequestHandlerParams } from 'express-serve-static-core';
import { Server } from 'http';

export declare type ServerEngine = (path: string, options: object, callback: (e: any, rendered: string) => void) => void;

export interface ServerOptions {
  host?:string;
  port?:number;
  docroot?:string;
  watch?:{ path:string, options?:WatcherOptions };
}

export interface WatcherOptions {
  persistent?:boolean;
  ignored?:string[];
  ignoreInitial?:boolean;
  followSymLinks?:boolean;
  cwd?:string;
  disableGlobbing?:boolean;
  usePolling?:boolean;
  interval?:number;
  binaryInterval?:number;
  useFsEvents?:boolean;
  alwaysStat?:boolean;
  depth?:number;
  awaitWriteFinish?:boolean|{ stabilityThreshold?:number, pollInterval?:number };
  ignorePermissionErrors?:boolean;
  atomic?:boolean;
}

export class ExpressServer {
  app:ExpressApplication = express();

  /**
   * Configures a basic express server.
   *
   * Usage:
   *
   * let engine = ngExpressEngine({
   *   bootstrap: AppServerModule,
   *   providers: [ provideModuleMap(exports['LAZY_MODULE_MAP']) ]
   * });
   *
   * let server = new ExpressServer(engine, { docroot: 'dist', host: 'www.example.com', port: 4200 });
   *
   * server.start();
   */
  constructor(private engine:ServerEngine, public options:ServerOptions = {}, public middleware:RequestHandlerParams[] = []) {
    // merge default options
    this.options = { host: 'localhost', port: 3000, docroot: 'dist', ...options };

    this.app.engine('html', engine);
    this.app.set('view engine', 'html');
    this.app.set('views', this.options.docroot);

    // enable compression
    this.app.use(compression());

    // enable CORS
    let corsOptions = {
      origin: true,
      methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
      allowedHeaders: ['Content-Type', 'authorization'],
    };

    this.app.use(cors(corsOptions));
    this.app.options('*', cors(corsOptions));

    // populate req.body
    this.app.use(bodyParser.json({ limit: '50mb' }));

    // register user-supplied middleware
    middleware.forEach(handler => this.app.use(handler));

    // serve static content
    this.app.all(/\.(html|js|json|css|map|ico|gif|jpg|png|wsdl|txt|php)$/, express.static(this.options.docroot, {
      fallthrough: false,
    }));

    // render everything else through ngExpressEngine
    this.app.use((req:Request, res:Response, next:Function) => {
      try {
        res.render('index', { req, res }, (err, html) => {
          if(err) {
            throw err;
          }

          let matches = html.match(/<ng-backend-data>(.*)<\/ng-backend-data>/m);

          if(Array.isArray(matches) && matches.length > 1) {
            res.type('application/json').send(matches[1]);
          } else {
            res.send(html);
          }
        });
      } catch(err) {
        console.error(err);
        next(err);
      }
    });

    // handle errors
    this.app.use((err, req:Request, res:Response, next:Function) => {
      if (res.headersSent) {
        return next(err);
      }

      res.status(err.statusCode).send(`<html>
          <head><title>Error</title></head>
          <body><h1>${err}</h1></body>
        </html>`);
    });

    this.start().then((server:Server) => {
      console.log(`Server is listening on http://${this.options.host}:${this.options.port}/`)

      if(this.options.watch) {
        this.watch(this.options.watch.path, this.options.watch.options);
      }
    });
  }

  start():Promise<Server> {
    return new Promise<Server>((resolve, reject) => {
      try {
        let server = this.app.listen(this.options.port, this.options.host, () => resolve(server));
      } catch(err) {
        reject(err);
      }
    });
  }

  watch(path:string, options:WatcherOptions) {
    console.log(`Watching ${path} for changes.`);

    chokidar.watch(path, options).on('all', (event, path) => {
      console.log(`Detected ${event} event on ${path.replace(/\\/g, '/')}, exiting.`);
      process.exit(0);
    });
  }
}


