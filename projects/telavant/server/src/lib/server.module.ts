import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { ServerConfigProvider } from './config.service';
import { MailServerModule } from './mail/mail.module';

@NgModule({
  declarations: [ ],
  imports: [
    ServerModule,
    ServerTransferStateModule,
    MailServerModule
  ],
  exports: [
    ServerModule,
    ServerTransferStateModule,
    MailServerModule
  ],
  providers: [
    ServerConfigProvider
  ]
})
export class TelavantServerModule { }
