import { FactoryProvider } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { AppConfig, APP_CONFIG } from '@telavant/platform';
import { readFileSync } from "fs";

export function loadConfiguration(config:AppConfig, state:TransferState):AppConfig {
  Object.assign(config, JSON.parse(readFileSync('conf/config.json').toString('utf-8')));

  const stateKey = makeStateKey<AppConfig>('AppConfig');
  state.set<AppConfig>(stateKey, config);

  return config;
}

export const ServerConfigProvider:FactoryProvider = {
  provide: APP_CONFIG,
  useFactory: loadConfiguration,
  deps: [ AppConfig, TransferState ]
};
