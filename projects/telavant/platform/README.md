# @telavant/platform

This library provides additional functionality for Angular Universal apps. It includes a built-in but
extendable [Express](https://github.com/expressjs/express) server, which is packaged separately as
`@telavant/server`; backend database support via [TypeORM](https://github.com/typeorm/typeorm); classes
to support backend data resources in both server and browser contexts; and a custom
[Schematic](https://angular.io/guide/schematics) to install the library in your project and generate
frontend, backend, and admin components for data resources.

This package was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## TL;DR

`ng new appname`<br>
`cd appname`<br>
`ng add @telavant/platform`<br>
`ng generate config-mysql`<br>
`ng generate resource example`

## Install

Run `ng add @telavant/platform` from your project folder. It will set itself up as the default
schematic for the project. Since `@telavant/platform` extends `@schematics/angular`, you will still
have access to the standard Angular generators.

By default `src/app/app.server.module.ts` will be installed at or moved to `src/server/server.module.ts`
and a backend routing module will be created as `src/server/server-routing.module.ts`. To change this,
use the `--server-dir` option. The base URI for API calls defaults to `/api` but can be changed with
`--backend-path`.

An admin module will also be created, unless otherwise specified by using `--skip-admin`. Its location,
which by default is at `src/admin`, can be changed via `--admin-dir`. The URI for the admin component
defaults to `/admin` but can be changed with `--admin-path`.

Additional options are the same as for the `@schematics/angular:universal` generator. Use `--dry-run`
first to preview the changes that will be made.

## Configuration

You can [configure TypeORM](https://github.com/typeorm/typeorm/blob/master/docs/connection-options.md)
by editing `ormconfig.json`, or by using one of the configuration generators provided by `@telavant/platform`.
Currently, MySQL and SQLite are supported.

Examples:
<br>`ng generate config-sqlite development --database=mydb.sql`
<br>`ng generate config-mysql production --host=db.example.com --port=3306 --username=example --database=prod --password=xxx`

The development configuration will be used by default, unless you build for production.

## Data Resources

Create a data resource like this:

`ng generate resource example`

This will create files in `src/app/example`, unless another location is specified with `--path` or a
flattened file structure is requested with `--flat`. This includes an entity class named Example at
`src/app/example/example.entity.ts` which will be exported from `src/entities.ts`. The entity class
defines the schema for the data resource, and the underlying storage (e.g. a MySQL table) will be
automatically updated to reflect this structure when the app is started.

An ExampleService class will be created as `example.service.ts` and can be used to access the backend
database from either server or browser context. If called from the server, the database is queried
directly with [TypeORM](https://github.com/typeorm/typeorm), and from the browser, it will communicate
with the backend version of itself over a REST interface.

Frontend, backend, and admin components will be also created at that location, and routes will be
added to AppRoutingModule, ServerRoutingModule, and AdminRoutingModule respectively.

## Sending Mail

A backend component to handle sending email over SMTP can be generated via the CLI:

`ng generate mail`



## Building

Run `npm start` to build both the browser and server bundles and start the server script when the build
is complete.

## Soon

* improved build scripts with file watchers and automatic server restarting when changes are detected
* generate admin CRUD interface and front-end display with `ng generate resource`
* generators for website features: authentication, blog/forums, ecommerce, third party services, etc.
* backend email service
