import { AfterViewInit, Component, ElementRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

export declare type FieldType = 'input' | 'select' | 'textarea';

export interface FieldOption {
  value:string,
  label:string,
  icon?:string;
  checked?:boolean;
  disabled?:boolean;
}

@Component({
  selector: 'field',
  templateUrl: './field.component.html',
})
export class FieldComponent implements AfterViewInit {
  @Input() form:FormGroup;
  @Input() name:string;
  @Input() label:string;
  @Input() labelWidth:number = 1;
  @Input() fieldWidth:number = 12;
  @Input() placeholder:string;
  @Input() type:FieldType;
  @Input() options:FieldOption[];

  constructor(private el:ElementRef) {}

  ngAfterViewInit() {
    let nativeElement:HTMLElement = this.el.nativeElement;
    let parentElement:HTMLElement = nativeElement.parentElement;

    // move all children out of the element
    while(nativeElement.firstChild) {
      parentElement.insertBefore(nativeElement.firstChild, nativeElement);
    }

    // remove the empty element(the host)
    parentElement.removeChild(nativeElement);
  }
}
