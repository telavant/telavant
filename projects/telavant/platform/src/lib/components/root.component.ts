import { Component } from '@angular/core';

/**
 * RootComponent is provided to aid in supporting multiple site layouts,
 * freeing AppComponent up for use as the default layout.
 */

@Component({ selector: 'app-root', template: '<router-outlet></router-outlet>' })
export class RootComponent { }
