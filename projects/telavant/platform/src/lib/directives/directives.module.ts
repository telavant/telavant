import { NgModule } from '@angular/core';
import {
  GridColumnDirective, GridColDirective, GridLGColumnDirective, GridMDColumnDirective, GridOffsetDirective,
  GridPullDirective, GridPushDirective, GridSMColumnDirective, GridXLColumnDirective, GridXSColumnDirective,
  GridRowDirective,
} from './grid.directives';

@NgModule({
  declarations:[
    GridRowDirective,
    GridColumnDirective,
    GridXSColumnDirective,
    GridSMColumnDirective,
    GridMDColumnDirective,
    GridLGColumnDirective,
    GridXLColumnDirective,
    GridColDirective,
    GridOffsetDirective,
    GridPushDirective,
    GridPullDirective,
  ],
  exports:[
    GridRowDirective,
    GridColumnDirective,
    GridXSColumnDirective,
    GridSMColumnDirective,
    GridMDColumnDirective,
    GridLGColumnDirective,
    GridXLColumnDirective,
    GridColDirective,
    GridOffsetDirective,
    GridPushDirective,
    GridPullDirective,
  ],
  providers:[],
})
export class DirectivesModule {
}
