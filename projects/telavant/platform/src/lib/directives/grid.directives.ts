import { Directive, ElementRef, Renderer2, Input, OnInit } from '@angular/core';

export interface GridOptions {
  col:number;
  offset?:number;
  push?:number;
  pull?:number;
}

export type ColumnOptions = number | GridOptions;

// @dynamic
export abstract class GridDirectiveBase {
  constructor(private element:ElementRef, private renderer:Renderer2) { }

  protected addClasses(newClasses:string[]) {
    let oldClasses:string[] = this.element.nativeElement.className.split(/\s+/);
    let classes:string[] = oldClasses.concat(newClasses).filter((value, index, self) => self.indexOf(value) === index);
    this.renderer.setAttribute(this.element.nativeElement, 'class', classes.join(' '));
  }

  protected addClass(newClass:string) {
    this.addClasses([newClass]);
  }
}

// @dynamic
export abstract class ResponsiveColumnDirective extends GridDirectiveBase implements OnInit {
  abstract value:ColumnOptions;

  constructor(private e:ElementRef, private r:Renderer2, private attribute:string) { super(e, r) }

  public ngOnInit() {
    this.value = typeof this.value == 'object' ? this.value : <GridOptions>{ col: this.value };

    this.addClasses(Object.keys(this.value).map(key => {
      let cls = 'clr-' + key;

      if(this.attribute && this.attribute != 'xs') {
        cls += '-' + this.attribute;
      }

      if(this.value[key]) {
        cls += '-' + this.value[key];
      }

      return cls;
    }));
  }
}

// @dynamic
export abstract class StaticColumnDirective extends GridDirectiveBase implements OnInit {
  abstract value:number;

  constructor(private e:ElementRef, private r:Renderer2, private attribute:string) { super(e, r) }

  public ngOnInit() {
    this.addClass(this.attribute + '-' + this.value);
  }
}

@Directive({ selector: 'row' })
export class GridRowDirective extends GridDirectiveBase {
  constructor(e:ElementRef, r:Renderer2) {
    super(e, r);

    this.addClass('clr-row');
  }
}

@Directive({ selector: 'column' })
export class GridColumnDirective { }

@Directive({ selector: '[xs]' })
export class GridXSColumnDirective extends ResponsiveColumnDirective {
  @Input('xs') value:ColumnOptions;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'xs') }
}

@Directive({ selector: '[sm]' })
export class GridSMColumnDirective extends ResponsiveColumnDirective {
  @Input('sm') value:ColumnOptions;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'sm') }
}

@Directive({ selector: '[md]' })
export class GridMDColumnDirective extends ResponsiveColumnDirective {
  @Input('md') value:ColumnOptions;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'md') }
}

@Directive({ selector: '[lg]' })
export class GridLGColumnDirective extends ResponsiveColumnDirective {
  @Input('lg') value:ColumnOptions;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'lg') }
}

@Directive({ selector: '[xl]' })
export class GridXLColumnDirective extends ResponsiveColumnDirective {
  @Input('xl') value:ColumnOptions;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'xl') }
}

@Directive({ selector: '[col]' })
export class GridColDirective extends StaticColumnDirective {
  @Input('col') value:number;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'col') }
}

@Directive({ selector: '[offset]' })
export class GridOffsetDirective extends StaticColumnDirective {
  @Input('offset') value:number;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'offset') }
}

@Directive({ selector: '[push]' })
export class GridPushDirective extends StaticColumnDirective {
  @Input('push') value:number;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'push') }
}

@Directive({ selector: '[pull]' })
export class GridPullDirective extends StaticColumnDirective {
  @Input('pull') value:number;
  constructor(e:ElementRef, r:Renderer2) { super(e, r, 'pull') }
}
