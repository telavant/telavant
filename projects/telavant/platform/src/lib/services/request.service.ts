import { Inject, Injectable, Optional } from '@angular/core';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { ParamsDictionary, Request } from 'express-serve-static-core';
import { IncomingHttpHeaders } from 'http';

@Injectable({ providedIn: 'root' })
export class RequestService {
  constructor(@Optional() @Inject(REQUEST) public request:Request) { }

  getProtocol = ():string => this.request.protocol;
  getMethod   = ():string => this.request.method;
  getHost     = ():string => this.request.hostname;
  getUrl      = ():string => this.request.url;
  getQuery    = ():any    => this.request.query;
  getParams   = ():ParamsDictionary    => this.request.params;
  getParam    = (key:string):string    => this.request.params[key];
  getHeaders  = ():IncomingHttpHeaders => this.request.headers;
  getHeader   = (key:string):string    => this.request.header(key);
  getCookies  = ():any => this.request.cookies;
  getBody     = ():any => this.request.body;
}
