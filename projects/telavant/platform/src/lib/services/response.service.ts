import { Inject, Injectable, Optional } from '@angular/core';
import { RESPONSE } from '@nguniversal/express-engine/tokens';
import { Response } from 'express-serve-static-core';
import { ms } from 'ms';

export type HttpCacheDirective =
  'public'
  | 'private'
  | 'no-store'
  | 'no-cache'
  | 'must-revalidate'
  | 'no-transform'
  | 'proxy-revalidate';

/**
 * Based on ServerResponseService from fusebox-angular-universal-starter
 *
 * @see https://github.com/patrickmichalina/fusebox-angular-universal-starter/blob/master/src/client/app/shared/services/server-response.service.ts
 */
@Injectable({ providedIn: 'root' })
export class ResponseService {
  constructor(@Optional() @Inject(RESPONSE) public response:Response) { }

  getHeader(key:string):string {
    return this.response.getHeader(key) as string;
  }

  setHeader(key:string, value:string):this {
    if(this.response) {
      this.response.header(key, value);
    }

    return this;
  }

  appendHeader(key:string, value:string, delimiter = ','):this {
    if(this.response) {
      const current = this.getHeader(key);
      if(!current) return this.setHeader(key, value);

      const newValue = [...current.split(delimiter), value]
        .filter((el, i, a) => i === a.indexOf(el))
        .join(delimiter);

      this.response.header(key, newValue);
    }

    return this;
  }

  setHeaders(dictionary:{ [key:string]:string }):this {
    if(this.response) {
      Object.keys(dictionary).forEach(key => this.setHeader(key, dictionary[key]));
    }

    return this;
  }

  setStatus(code:number, message?:string):this {
    if(this.response) {
      this.response.statusCode = code;

      if(message) {
        this.response.statusMessage = message;
      }
    }

    return this;
  }

  setUnauthorized = (message = 'Unauthorized'):this   => this.setStatus(401, message);
  setNotFound = (message = 'Page not found'):this     => this.setStatus(404, message);
  setError = (message = 'Internal server error'):this => this.setStatus(500, message);

  setCachePrivate():this {
    if(this.response) {
      this.setCache('private');
    }

    return this;
  }

  setCacheNone():this {
    if(this.response) {
      this.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
      this.setHeader('Pragma', 'no-cache');
    }

    return this;
  }

  setCache(directive:HttpCacheDirective, maxAge?:string, smaxAge?:string):this {
    if(this.response) {
      const smax = smaxAge ? `, s-maxage=${ms(smaxAge) / 1000}` : '';
      this.setHeader('Cache-Control', `${directive}, max-age=${maxAge ? ms(maxAge) / 1000 : 0}${smax}`);
      this.setHeader('Expires', new Date(Date.now() + (maxAge ? ms(maxAge) : 0)).toUTCString());
    }

    return this;
  }
}
