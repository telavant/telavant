import { FactoryProvider, Injectable, InjectionToken } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';

@Injectable({ providedIn: 'root' })
export class AppConfig {
  name:string;
}

export const APP_CONFIG = new InjectionToken<string>('AppConfig');

export function loadConfiguration(config:AppConfig, state:TransferState):AppConfig {
  const stateKey = makeStateKey<AppConfig>('AppConfig');
  return Object.assign(config, state.get<AppConfig>(stateKey, null));
}

export const AppConfigProvider:FactoryProvider = {
  provide: APP_CONFIG,
  useFactory: loadConfiguration,
  deps: [ AppConfig, TransferState ]
};
