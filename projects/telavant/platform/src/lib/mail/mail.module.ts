import { NgModule } from '@angular/core';
import { MailConfigProvider } from './mail.config';

@NgModule({
  declarations: [ ],
  imports: [ ],
  exports: [ ],
  providers: [
    MailConfigProvider
  ]
})
export class MailModule {
}
