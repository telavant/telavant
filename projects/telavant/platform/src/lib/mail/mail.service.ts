import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MailConfig, MailMessage, MailResponse } from './mail.config';

@Injectable({ providedIn: 'root' })
export class MailService {
  constructor(private http:HttpClient, private config:MailConfig) { }

  public sendMail(message:MailMessage):Promise<MailResponse> {
    return this.http.post<MailResponse>(this.config.endpoint, message).toPromise();
  }
}
