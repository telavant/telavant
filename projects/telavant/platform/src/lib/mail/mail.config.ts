import { FactoryProvider } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';

export class MailConfig {
  endpoint:string;
}

export function loadMailConfig(state:TransferState):MailConfig {
  const stateKey = makeStateKey<MailConfig>('MailConfig');
  return state.get<MailConfig>(stateKey, null);
}

export const MailConfigProvider:FactoryProvider = {
  provide: MailConfig,
  useFactory: loadMailConfig,
  deps: [ TransferState ],
};

export interface MailMessage {
  from?: string;
  to?: string;
  subject: string;
  body: string;
}

export interface MailResponse {
  type: 'success' | 'error';
  message: string;
  data: any;
}
