import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FieldComponent } from './components/field.component';
import { RootComponent } from './components/root.component';
import { DirectivesModule } from './directives/directives.module';
import { MailModule } from './mail/mail.module';
import { AppConfigProvider } from './services/config.service';

@NgModule({
  declarations: [
    RootComponent,
    FieldComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    DirectivesModule,
    MailModule
  ],
  exports: [
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    DirectivesModule,
    MailModule
  ],
  providers: [
    AppConfigProvider,
  ]
})
export class TelavantPlatformModule { }
