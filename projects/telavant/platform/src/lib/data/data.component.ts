import { OnInit } from '@angular/core';
import { RequestService } from '../services/request.service';
import { DataService, ResultsWithTotal } from './data.service';
import { DataModel } from './data.model';

export interface OnGet    { ngOnGet(); }
export interface OnPost   { ngOnPost(); }
export interface OnPut    { ngOnPut(); }
export interface OnDelete { ngOnDelete(); }

export declare type DataResult<T> = T|T[]|ResultsWithTotal<T>|boolean;

export abstract class DataComponent<T extends DataModel<T>> implements OnInit, OnGet, OnPost, OnPut, OnDelete {
  id:number;
  body:any;
  data:string;

  protected constructor(protected service:DataService<T>, protected request:RequestService) {
    this.id   = +request.getParam('id');
    this.body = request.getBody();
  }

  async ngOnInit() {
    let hook:string;

    switch(this.request.getMethod()) {
      case 'POST':   hook = 'ngOnPost';   break;
      case 'PUT':    hook = 'ngOnPut';    break;
      case 'DELETE': hook = 'ngOnDelete'; break;
      default:       hook = 'ngOnGet';
    }

    if(typeof this[hook] === 'function') {
      this.data = JSON.stringify(await this[hook]());
    }
  }

  ngOnGet():Promise<DataResult<T>> {
    return this.id
      ? this.service.read(this.id)
      : this.service.list();
  }

  ngOnPost():Promise<DataResult<T>> {
    if(this.id) {
      throw new Error(`Not implemented: ${this.request.getMethod()} on resource ${this.request.getUrl()}`);
    }

    return this.service.find(this.body);
  }

  ngOnPut():Promise<DataResult<T>> {
    return this.id
      ? this.service.update(this.body)
      : this.service.create(this.body);
  }

  ngOnDelete():Promise<DataResult<T>> {
    if(!this.id) {
      throw new Error(`Not implemented: ${this.request.getMethod()} on resource ${this.request.getUrl()}`);
    }

    return this.service.remove(this.body);
  }
}
