import { Inject, Injectable, Optional } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { Request } from 'express-serve-static-core';
import { MacroTaskWrapperService } from '../services/macrotaskwrapper.service';

export interface DataRequest {
  id:number;
  request:Request;
}

@Injectable({ providedIn: 'root' })
export class DataResolver implements Resolve<DataRequest> {
  constructor(@Optional() @Inject(REQUEST) private request:Request) {}

  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):DataRequest {
    return {
      id: route.paramMap.has('id') ? +route.paramMap.get('id') : null,
      request: this.request,
    };
  }
}
