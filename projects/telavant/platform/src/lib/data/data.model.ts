import { Type } from 'class-transformer';
import { BaseEntity, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

export declare type DefaultProperties<T> = Partial<T>|{};

export const DateType = _ => Date;

// @dynamic
export abstract class DataModel<T> extends BaseEntity {
  constructor(properties:DefaultProperties<T> = {}) {
    super();

    for(let prop in properties) {
      if(properties.hasOwnProperty(prop)) {
        this[prop] = properties[prop];
      }
    }

    if(!this.id) {
      delete this['id'];
    }
  }

  pick<T, K extends keyof T>(keys:string[]):Pick<T, K> {
    return <Pick<T, K>>keys.reduce((p, c) => { return { ...p, [c]: this[c] } }, {});
  }

  @PrimaryGeneratedColumn()
  id?:number = null;
}

// @dynamic
export abstract class DateModel<T> extends DataModel<T> {
  @Type(DateType)
  @CreateDateColumn()
  created?:Date = new Date();

  @Type(DateType)
  @UpdateDateColumn({ nullable: true })
  updated?:Date = new Date();
}
