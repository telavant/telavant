import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { ClrDatagridFilterInterface, ClrDatagridStateInterface } from '@clr/angular';
import { plainToClass } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { FindManyOptions, FindOneOptions, FindOperator, getRepository, Repository, TreeRepository } from 'typeorm';
import { MacroTaskWrapperService } from '../services/macrotaskwrapper.service';
import { DataModel } from './data.model';

export type Dataset<T> = { [k:number]:T };

export interface Option {
  value:string,
  label:string,
  icon?:string;
  checked?:boolean;
  disabled?:boolean;
}

export type Options = Option[];

export interface DataPageInterface {
  from?:number;
  to?:number;
  size?:number;
}

export interface DataSortInterface {
  by:string;
  reverse:boolean;
}

export interface DataFilterInterface {
  [key:string]:string|FindOperator<any>;
}

export type DatagridSortFilter = { by:string, reverse:boolean };
export type DatagridStaticFilter = { property:string; value:string; };
export type DatagridFilter<T = any> = DatagridStaticFilter | ClrDatagridFilterInterface<T>;

export type ServerBackend<T extends DataModel<T>> = Repository<T> | TreeRepository<T>;
export type ClientBackend<T extends DataModel<T>> = DataResource<T>;

export type ResultsWithTotal<T> = [T[], number];

export interface CrudInterface<T extends DataModel<T>> {
  list():Promise<ResultsWithTotal<T>>;
  find(conditions?:FindManyOptions<T>):Promise<ResultsWithTotal<T>>;
  read(id:number, options?:FindOneOptions<T>):Promise<T>;
  create(data:T):Promise<T>;
  update(data:T):Promise<T>;
  remove(data:T):Promise<boolean>;
}

export class DataResource<T extends DataModel<T>> implements CrudInterface<T> {
  private httpOptions = { withCredentials: true };

  constructor(
    private entity:ClassType<T>,
    protected http:HttpClient,
    private endpoint:string
  ) {}

  list():Promise<ResultsWithTotal<T>> {
    return this.find();
  }

  find(conditions?:FindManyOptions<T>):Promise<ResultsWithTotal<T>> {
    return this.http.post<ResultsWithTotal<T>>(this.endpoint, conditions, this.httpOptions).pipe(
      map(([results, total]) => <ResultsWithTotal<T>>[
        plainToClass(this.entity, results),
        Number.isInteger(total) ? total : results.length
      ])
    ).toPromise();
  }

  read(id:number, conditions?:FindOneOptions<T>):Promise<T> {
    return this.http.get<T>(this.endpoint + '/' + id, this.httpOptions).pipe(
      map(result => plainToClass(this.entity, result))
    ).toPromise<T>();
  }

  create(data:T):Promise<T> {
    return this.http.put<T>(this.endpoint, data, this.httpOptions).pipe(
      map(result => plainToClass(this.entity, result))
    ).toPromise<T>();
  }

  update(data:T):Promise<T> {
    return this.http.put<T>(this.endpoint + '/' + data.id, data, this.httpOptions).pipe(
      map(result => plainToClass(this.entity, result))
    ).toPromise<T>();
  }

  remove(data:T):Promise<boolean> {
    return this.http.delete<T>(this.endpoint + '/' + data.id, this.httpOptions).pipe(
      map(result => true)
    ).toPromise<boolean>();
  }
}

@Injectable()
export abstract class DataService<T extends DataModel<T>> implements CrudInterface<T> {
  // set by @BackendData() decorator
  public entity:ClassType<T>;
  public uri:string;

  // public response:Response;

  protected dataset:Dataset<T> = <Dataset<T>>{};
  private subject:ReplaySubject<Dataset<T>> = new ReplaySubject<Dataset<T>>();
  public data$:Observable<T[]>;

  private get server():ServerBackend<T> {
    try {
      return getRepository<T>(this.entity);
    } catch(err) {
      return null;
    }
  }

  private get client():ClientBackend<T> {
    return this.server ? null : new DataResource<T>(this.entity, this.http, this.uri);
  }

  protected constructor(private http:HttpClient, public state:TransferState, public macrotasks:MacroTaskWrapperService) {
    this.data$ = this.subject.asObservable().pipe(
      map(dataset => Object.keys(dataset).map(key => dataset[key]))
    );
  }

  next(values:Dataset<T>|T[]) {
    if(Array.isArray(values)) {
      values.forEach(data => this.dataset[data.id] = data);
    } else {
      this.dataset = { ...this.dataset, ...values };
    }

    this.subject.next(this.dataset);
  }

  protected hash(str:string):string {
    let hash = 0;

    for(let i = 0; i < str.length; i++) {
      let char = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + char;
      hash = hash & hash; // Convert to 32bit integer
    }

    return hash.toString(16);
  }

  list():Promise<ResultsWithTotal<T>> {
    return this.find();
  }

  async find(options?:FindManyOptions<T>):Promise<ResultsWithTotal<T>> {
    const taskId = this.hash(`DataService.find():${this.uri}:${JSON.stringify(options)}`);
    const stateKey = makeStateKey<T>(taskId);
    let results:ResultsWithTotal<T> = this.state.get<ResultsWithTotal<T>>(stateKey, null);

    if(!results) {
      if(this.client) {
        results = await this.client.find(options);
      } else {
        results = await this.macrotasks.wrapMacroTask<ResultsWithTotal<T>>(taskId, this.server.findAndCount(options));
        await this.macrotasks.awaitMacroTasks(taskId);
      }

      this.state.set<ResultsWithTotal<T>>(stateKey, results);
    }

    this.next(results[0]);
    return results;
  }

  async read(id:number, options?:FindOneOptions<T>):Promise<T|undefined> {
    const taskId = this.hash(`DataService.read(${id}):${this.uri}`);
    const stateKey = makeStateKey<T>(taskId);
    let result:T = this.state.get<T>(stateKey, null);

    if(!result) {
      if(this.client) {
        result = await this.client.read(id, options);
      } else {
        result = await this.macrotasks.wrapMacroTask<T>(taskId, this.server.findOne(id, options));
        await this.macrotasks.awaitMacroTasks(taskId);
      }

      this.state.set<T>(stateKey, result);
    }

    this.next({ [id]: result });
    return result;
  }

  async create(data:T):Promise<T> {
    let result:T;

    if(this.client) {
      result = await this.client.create(data);
    } else {
      const taskId = this.hash('DataService.create()');
      result = await this.macrotasks.wrapMacroTask<T>(taskId, this.server.save({ ...<any>data }));
      await this.macrotasks.awaitMacroTasks(taskId);
    }

    this.next([result]);
    return result;
  }

  async update(data:T):Promise<T> {
    if(this.client) {
      data = await this.client.update(data);
    } else {
      const taskId = this.hash('DataService.update()');
      await this.macrotasks.wrapMacroTask<T>(taskId, this.server.save({ ...<any>data }));
      await this.macrotasks.awaitMacroTasks(taskId);
    }

    this.next([data]);
    return data;
  }

  save(data:T):Promise<T> {
    return 'id' in data
      ? this.update(data)
      : this.create(data);
  }

  async remove(data:T):Promise<boolean> {
    if(this.client) {
      await this.client.remove(data);
    } else {
      const taskId = this.hash('DataService.remove()');
      await this.macrotasks.wrapMacroTask<T>(taskId, this.server.remove(data));
      await this.macrotasks.awaitMacroTasks(taskId);
    }

    let { [data.id]: removed, ...dataset } = this.dataset;
    this.dataset = dataset;
    this.subject.next(this.dataset);

    return Promise.resolve(true);
  }


  loadCondition(obj):FindOperator<T>|{}|string {
    if(typeof obj !== 'object') {
      return obj;
    }

    if('_type' in obj) {
      return plainToClass(FindOperator, obj);
    }

    Object.keys(obj).forEach(prop => {
      obj[prop] = typeof obj[prop] == 'object' ? this.loadCondition(obj[prop]) : obj[prop];
    });

    return obj;
  }

  loadOptions(options:FindManyOptions<T>, extra?:FindManyOptions<T>):FindManyOptions<T> {
    if(!options) {
      options = {};
    }

    if(options.where) {
      options.where = this.loadCondition(options.where);
    }

    options = { ...extra, ...options };
    return options;
  }

  convertDatagridOptions({ page, sort, filters }:ClrDatagridStateInterface<T>, findOptions:FindManyOptions<T> = {}):FindManyOptions<T> {
    if(page) {
      if(typeof page.from != 'undefined') {
        findOptions.skip = page.from;
      } else {
        page.from = 0;
      }

      if(typeof page.size != 'undefined') {
        findOptions.take = page.size;
      } else if(typeof page.to != 'undefined') {
        findOptions.take = page.to - page.from;
      }
    }

    if(sort && typeof sort.by != 'undefined') {
      findOptions.order = {};
      findOptions.order[sort.by as string] = sort.reverse ? 'DESC' : 'ASC';
    }

    if(filters) {
      let conditions = [];

      filters.forEach(filter => {
        let property = filter['property'] || filter['filterFn']['property'];

        if(property) {
          let properties = Array.isArray(property) ? property : [property];
          let columns = [];

          if('join' in findOptions && 'alias' in findOptions.join) {
            let prefix = findOptions.join.alias + '.';
            properties.map(property => property.indexOf('.') == -1 ? prefix + property : property);
          }

          if('value' in filter) {
            columns = properties.map(property => property + ' LIKE "%' + filter.value + '%"');
          } else if('property' in filter && 'options' in filter) {
            properties.forEach(property => {
              filter.options.filter(item => item.checked).forEach(item => {
                columns.push(`${property} = "${item.value}"`)
              });
            });
          }

          if(columns.length > 0) {
            conditions.push(columns.length > 1 ? '(' + columns.join(' OR ') + ')' : columns[0]);
          }
        }
      });

      findOptions.where = conditions.join(' AND ');
    }

    return findOptions;
  }
}
