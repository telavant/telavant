import { DataService } from './data.service';

export function DataBackend(entity:any, uri:string):ClassDecorator {
  return function(target:any) {
    if(target.prototype instanceof DataService) {
      Object.defineProperty(target.prototype, 'entity', { value: entity });
      Object.defineProperty(target.prototype, 'uri', { value: uri });
    } else {
      throw new Error("@DataBackend decorator can only be used on classes that extend DataService.");
    }
  };
}
