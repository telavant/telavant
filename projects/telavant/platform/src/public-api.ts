/*
 * Public API surface of @telavant/platform
 */

export * from './lib/components/root.component';
export * from './lib/components/field.component';
export * from './lib/directives/directives.module';
export * from './lib/directives/grid.directives';
export * from './lib/data/data.component';
export * from './lib/data/data.decorator';
export * from './lib/data/data.model';
export * from './lib/data/data.resolver';
export * from './lib/data/data.service';
export * from './lib/services/config.service';
export * from './lib/services/macrotaskwrapper.service';
export * from './lib/mail/mail.config';
export * from './lib/mail/mail.service';
export * from './lib/services/request.service';
export * from './lib/services/response.service';
export * from './lib/platform.module';

export interface AdminInterface { }

export type AdminNavigation = {
  path: string,
  icon?: string
  label: string,
};
